jQuery(document).ready(function () {
    // code start for geeting sizes from php
blkfrm=100;
brownfrm=150;
strechedcanvasfrm=200;
grayscaleprice=101;
invertprice=102;
sepiaprice=103;
sepia2price=104;
embossprice=105;
blendprice=106;
majorprice=500;
minorprice=400;



// code end for geeting sizes from php

//code start to add filter in image ig grayscale etc etc..

var filters = ['grayscale', 'invert', 'sepia', 'sepia2', 'emboss', 'blend'];
 f = fabric.Image.filters;
var applyFilter = function(index, filter, obj, canvas, id11) {
  jQuery('#loader').modal('show');
    obj.filters[index] = filter;
obj.applyFilters(function () {
  canvas.renderAll();
    var mediumQuality = canvas.toDataURL("image/jpeg", 0.5);
    jQuery('#process_img_temp_view').attr('src', mediumQuality);
});

if(jQuery('#effects'+id11).length > 0 && jQuery('#effects'+id11).val() != ''){
        jQuery('#effects'+id11).val(filters[index]);
}else{
      jQuery('<input>').attr({
        type: 'hidden',
        id: 'effects'+id11,
        name: 'effects'+id11,
        value: filters[index]
    }).appendTo('#hidden_dynamic_fields');
}

   jQuery('#loader').modal('hide');
}


// code for 4 steps next button

   var navListItems = jQuery('div.setup-panel div a'),
            allWells = jQuery('.setup-content'),
            allNextBtn = jQuery('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = jQuery(jQuery(this).attr('href')),
                $item = jQuery(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = jQuery(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = jQuery('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        jQuery(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                jQuery(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });





    jQuery('div.setup-panel div a.btn-primary').trigger('click');

    jQuery('.btn-vertical-slider').on('click', function () {
        
        if (jQuery(this).attr('data-slide') == 'next') {
            jQuery('#myCarousel').carousel('next');
        }
        if (jQuery(this).attr('data-slide') == 'prev') {
            jQuery('#myCarousel').carousel('prev')
        }

    });

  jQuery('#media').carousel({
    pause: true,
    interval: false,
  });

// end code of next button


//code start for upload file

jQuery('.uploadimg').click(function(){
    document.getElementById("upfile").click();
});

var i=0;
jQuery("#upfile").change(function(evt) {
    jQuery('#loader').modal('show');
    jQuery('.errmsg').attr('style','display:none;');
    jQuery("#canvastype option").attr("disabled",false);
    if(i<13){
        var files = evt.target.files; // FileList object
        // Loop through the FileList and render image files as thumbnails.
    if(files.length>0){


            var f=files[0];
            // Only process image files.
    if (f.type.match('image.*')) {
        var reader = new FileReader();
            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    jQuery('.thumbnail ').removeClass('activeimage');
                    jQuery('#image'+i).attr('src',e.target.result);
                    jQuery('#image'+i).parent().addClass( "activeimage" );
                    jQuery('#image'+i).attr('name','image'+i);
                    jQuery('#image'+i).attr('alt',i);
                    jQuery('#singlefrm').attr('src',e.target.result);
                    setTimeout(function (){
                      // delayd execution for 1 sec.
                      set3frame(e.target.result);
                     
                    }, 1000);
                    drawcanvas(e.target.result);
                    
                    jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'originalimg'+i,
                        name: 'originalimg'+i,
                        value: e.target.result
                    }).appendTo('#hidden_dynamic_fields');
                    

                    if(jQuery('#view'+i).length > 0 && jQuery('#view'+i).val() != ''){
                        jQuery('#view'+i).val('LANDSCAPE');
                    }else{
                      jQuery('<input>').attr({
                                            type: 'hidden',
                                            id: 'view'+i,
                                            name: 'view'+i,
                                            value: 'LANDSCAPE'
                                        }).appendTo('#hidden_dynamic_fields');
                    }
                    // default values
                    jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'media'+i,
                        name: 'media'+i,
                        value: 'Canvas Media'
                    }).appendTo('#hidden_dynamic_fields');
                    jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'type'+i,
                        name: 'type'+i,
                        value: 'Rolled'
                    }).appendTo('#hidden_dynamic_fields');
                     jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'media'+i,
                        name: 'media'+i,
                        value: 'CANVAS MEDIA'
                    }).appendTo('#hidden_dynamic_fields');
                    jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'frame'+i,
                        name: 'frame'+i,
                        value: 'NO FRAME'
                    }).appendTo('#hidden_dynamic_fields');
                     jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'effects'+i,
                        name: 'effects'+i,
                        value: 'No effect'
                    }).appendTo('#hidden_dynamic_fields');
                     jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'brightness'+i,
                        name: 'brightness'+i,
                        value: '0/100'
                    }).appendTo('#hidden_dynamic_fields');
                     jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'contrast'+i,
                        name: 'contrast'+i,
                        value: '0/100'
                    }).appendTo('#hidden_dynamic_fields');

                    jQuery('#selected_form_img').attr('src', e.target.result );
                    jQuery('.radio2').prop("checked", false);
                    jQuery('.radio1').prop("checked", false);

                    jQuery('#zerorotation').click();
                };
            })(f);
            // Read in the image file as a data URL.
        reader.readAsDataURL(f);
        i=i+1;
    }else{
        alert("Oooops! Not A Image File!!!!...");
        jQuery('#loader').modal('hide');  
    }
  }else{
    jQuery('#loader').modal('hide');  
  }
 }else{
    alert("Sorry..!!! You can Upload Only 12 Files!!!!");
    jQuery('#loader').modal('hide');  
 }       

   
});



//code for draw canvas 
var canvasid=0;
var canvasarr=[];
function drawcanvas(blob) {

jQuery(".printposter").parent().attr("style", 'Display:none;');

         var imgObj = new Image();
        imgObj.src = blob;
        imgObj.onload = function () {
            // start fabricJS stuff
            
            var image = new fabric.Image(imgObj);
             image.set({
                    left: 0,
                    top: 0, 
                });
             
        var newCanvas = jQuery('<canvas/>',{
                     'class':'printposter',
                     id: 'myCanvas'+canvasid,
                     style: 'display: none;'                  
                }).prop({
                    width: image.width,
                    height: image.height
                });
        jQuery('#canvasprocessimage').append(newCanvas);
        canvas = new fabric.Canvas('myCanvas'+canvasid);

            canvas.add(image);
            canvas.setActiveObject(canvas.item(0));
             var object=canvas.getActiveObject();
            var filter = new fabric.Image.filters.Brightness({brightness:0
            });
            object.filters.push(filter);
            object.applyFilters(canvas.renderAll.bind(canvas));
            var filter1 = new fabric.Image.filters.Tint({
              color: '#000000',
              opacity: 0.0
            });
            object.filters.push(filter1);
            object.applyFilters(canvas.renderAll.bind(canvas));

            // end fabricJS stuff
            canvasarr[canvasid]=canvas;
            canvasid=canvasid+1;
            jQuery('#process_img_temp_view').attr('src',blob);

          if(parseInt(image.width)<3000 || parseInt(image.height)<2500){

              var warning_msg = 'Warning! your image size is '+image.width+' X '+image.height;
              if(parseInt(image.width)<3000 || parseInt(image.height)<2500){
                warning_msg = warning_msg + ' five frame ';
                jQuery("#canvastype option:contains('Five Frame')").attr("disabled","disabled");
              }
              if(parseInt(image.width)<2500 || parseInt(image.height) <2000){
                warning_msg = warning_msg + ' and three frame ';
                jQuery("#canvastype option:contains('Three Frame')").attr("disabled","disabled");
              }
              if(parseInt(image.width)<600 || parseInt(image.height) <600){
                warning_msg = warning_msg + ' and single frame ';
                jQuery("#canvastype option:contains('Single Frame')").attr("disabled","disabled");
              }
              warning_msg = warning_msg + ' is not applicable to this size';
              jQuery('.errmsg').removeAttr('style');
              jQuery('.errmsg').html(warning_msg);
          }

        }
}

//end of draw canvas


// code for cut image into 3 piece

function set3frame(blob) {
//start
var width;
var height;
var image = new Image();
image.src = blob;

image.onload = function(){
 width=image.width/3;
 height=image.height;
  cutImageUp();
};

function cutImageUp() {

    var imagePieces = [];
    for(var x = 0; x < 3; ++x) {
            var canvas = document.createElement('canvas');
            canvas.width = width;
            canvas.height = height;
            var context = canvas.getContext('2d');
            context.drawImage(image, x * width, 0, width, height, 0, 0, canvas.width, canvas.height);
          imagePieces.push(canvas.toDataURL());
        
    }
// imagePieces now contains data urls of all the pieces of the image
jQuery("#threefrm3").attr("src", imagePieces[2]);
jQuery("#twofrm3").attr("src", imagePieces[1]);
jQuery("#onefrm3").attr("src", imagePieces[0]);
 set5frame(blob); 
}
//end
}
// code to cut image into 5 piece

function set5frame(blob) {
//start
var width;
var height;
var image = new Image();
image.src = blob;

image.onload = function(){
 width=image.width/5;
 height=image.height;
  cutImageUp();
};

function cutImageUp() {

    var imagePieces = [];
    for(var x = 0; x < 5; ++x) {
            var canvas = document.createElement('canvas');
            canvas.width = width;
            canvas.height = height;
            var context = canvas.getContext('2d');
            context.drawImage(image, x * width, 0, width, height, 0, 0, canvas.width, canvas.height);
          imagePieces.push(canvas.toDataURL());
        
    }
// imagePieces now contains data urls of all the pieces of the image
jQuery("#fivefrm5").attr("src", imagePieces[4]);
jQuery("#fourfrm5").attr("src", imagePieces[3]);
jQuery("#threefrm5").attr("src", imagePieces[2]);
jQuery("#twofrm5").attr("src", imagePieces[1]);
jQuery("#onefrm5").attr("src", imagePieces[0]);
 jQuery('#loader').modal('hide');  
}
//end
}

// end of upload image code ...

// effects click events

var controls   = document.getElementById('controls');
controls.onchange = function()
{
  jQuery('#loader').modal('show');
  var id11=jQuery('.activeimage').children('img').attr("alt");
  id1=parseInt(id11)-1;
    var object=canvasarr[id1].getActiveObject();
      object.filters[0].brightness = parseInt(this.value);
      canvasarr[id1].renderAll();
      
object.applyFilters(function () {
  canvasarr[id1].renderAll();
    var mediumQuality = canvasarr[id1].toDataURL("image/jpeg", 0.5);
    jQuery('#process_img_temp_view').attr('src', mediumQuality);
     jQuery('#loader').modal('hide');
});
jQuery('#controls_text').html(parseInt(this.value));

if(jQuery('#brightness'+id11).length > 0 && jQuery('#brightness'+id11).val() != ''){
        jQuery('#brightness'+id11).val(parseInt(this.value)+'/100');
      }else{
            jQuery('<input>').attr({
              type: 'hidden',
              id: 'brightness'+id11,
              name: 'brightness'+id11,
              value: parseInt(this.value)+'/100'
          }).appendTo('#hidden_dynamic_fields');
      }
 
}


var controls1   = document.getElementById('controls1');
controls1.onkeyup = controls1.onchange = function()
{
  jQuery('#loader').modal('show');
   var id11=jQuery('.activeimage').children('img').attr("alt");
   id1=parseInt(id11)-1;
   console.log(id1);
   var object=canvasarr[id1].getActiveObject();
   val='0.'+parseInt(this.value);
    object.filters[1].opacity = val;

object.applyFilters(function () {
  canvasarr[id1].renderAll();
    var mediumQuality = canvasarr[id1].toDataURL("image/jpeg", 0.5);
    jQuery('#process_img_temp_view').attr('src', mediumQuality);
    jQuery('#loader').modal('hide');
});
jQuery('#controls_text1').html(parseInt(this.value));

if(jQuery('#contrast'+id11).length > 0 && jQuery('#contrast'+id11).val() != ''){
        jQuery('#contrast'+id11).val(parseInt(this.value)+'/100');
      }else{
            jQuery('<input>').attr({
              type: 'hidden',
              id: 'contrast'+id11,
              name: 'contrast'+id11,
              value: parseInt(this.value)+'/100'
          }).appendTo('#hidden_dynamic_fields');
      }
    
}
// end of canvas

jQuery('#grayscale').click(function(){
  

   var id11=jQuery('.activeimage').children('img').attr("alt");
   var id1=parseInt(id11)-1;
   var object=canvasarr[id1].getActiveObject();
   applyFilter(0, new f.Grayscale() , object, canvasarr[id1], id11);
});

jQuery('#invert').click(function(){
   var id11=jQuery('.activeimage').children('img').attr("alt");
   var id1=parseInt(id11)-1;
   var object=canvasarr[id1].getActiveObject();
   applyFilter(1, new f.Invert(), object, canvasarr[id1], id11);

});


jQuery('#sepiya').click(function(){
   var id11=jQuery('.activeimage').children('img').attr("alt");
   var id1=parseInt(id11)-1;
   var object=canvasarr[id1].getActiveObject();
    applyFilter(2, new f.Sepia(), object, canvasarr[id1], id11);

});


jQuery('#sepiya2').click(function(){
    var id11=jQuery('.activeimage').children('img').attr("alt");
   var id1=parseInt(id11)-1;
   var object=canvasarr[id1].getActiveObject();
   applyFilter(3, new f.Sepia2(), object, canvasarr[id1], id11);

});


jQuery('#emboss').click(function(){
    var id11=jQuery('.activeimage').children('img').attr("alt");
   var id1=parseInt(id11)-1;
   var object=canvasarr[id1].getActiveObject();

 applyFilter(4, new f.Convolute({
      matrix: [ 1,   1,  1,
                1, 0.7, -1,
               -1,  -1, -1 ]
    }), object, canvasarr[id1], id11);

});

jQuery('#blend').click(function(){
   var id11=jQuery('.activeimage').children('img').attr("alt");
   var id1=parseInt(id11)-1;
   var object=canvasarr[id1].getActiveObject();

applyFilter(5, new f.Blend({
      color: '#000AD8',
      mode: 'multiply'
    }), object, canvasarr[id1], id11);

});

//thumbnail section


// start of switch image code 

//jQuery('#loader').modal('show');
jQuery('.thumbnail').click(function(){
  //show loader
  jQuery('#loader').modal('show');
  jQuery('.errmsg').attr('style','display:none;');
  jQuery("#canvastype option").attr("disabled",false);

  jQuery('.thumbnail').removeClass('activeimage');
  jQuery(this).addClass('activeimage');
 //show error message to user if image is applicable or not on step 1
  var original_img_blob=jQuery(this).children('img').attr("src");
  var image = new Image();
  image.src = original_img_blob;
  image.onload = function () {
  if(parseInt(image.width)<3000 || parseInt(image.height)<2500){
    var warning_msg = 'Warning! your image size is '+image.width+' X '+image.height;
    if(parseInt(image.width)<3000 || parseInt(image.height)<2500){
        warning_msg = warning_msg + ' five frame ';

        jQuery("#canvastype option:contains('Five Frame')").attr("disabled","disabled");
              
    }
    if(parseInt(image.width)<2500 || parseInt(image.height) <2000){
      warning_msg = warning_msg + ' and three frame ';
      jQuery("#canvastype option:contains('Three Frame')").attr("disabled","disabled");
    }
    if(parseInt(image.width)<600 || parseInt(image.height) <600){
      warning_msg = warning_msg + ' and single frame ';
      jQuery("#canvastype option:contains('Single Frame')").attr("disabled","disabled");
    }
    warning_msg = warning_msg + ' is not applicable to this size';
    jQuery('.errmsg').removeAttr('style');
    jQuery('.errmsg').html(warning_msg);
  }
}

// setting first tab
  var alt=jQuery(this).children("img").attr('alt');
  var name=jQuery(this).children("img").attr('name');
  if(alt!=""){
    name=name.replace('image','');
    jQuery('.thumbnail ').removeClass('activeimage');
    jQuery(this).addClass( "activeimage" );

    //populate canvas image on tab 2
   var id11=jQuery('.activeimage').children('img').attr("alt");
   var id1=parseInt(id11)-1;
   var mediumQuality = canvasarr[id1].toDataURL("image/jpeg", 0.5);
   jQuery('#process_img_temp_view').attr('src', mediumQuality);
   jQuery('#img_preview').attr('src', mediumQuality);

    // set single frame 
    jQuery('#singlefrm').attr('src',original_img_blob);

    //set 3 frame 
    setTimeout(function (){
      // delayd execution for 1 sec.
      set3frame(original_img_blob);
    }, 1000);

    //set 5 frame 
    setTimeout(function (){
      // delayd execution for 1 sec.
      set5frame(original_img_blob);
    }, 1000);

// setting 4rth step
var frm=jQuery('#frame'+id11).val();
var size=jQuery('#size'+id11).val();
var type=jQuery('#type'+id11).val();
var pric=jQuery('#size_price'+id11).val();
var marginglobal=(jQuery('#preview_area').height()-jQuery('#img_preview').height())/2;

 
//assigning frame to step 3
jQuery('#frame_png').attr('src','');

  if (frm === undefined || frm === null) {
    jQuery(".radio1").prop("checked", false); 
    jQuery('#frame_png').attr('style','Display:none;');
  }else{
     frm=frm.replace(' ',''); 
    jQuery("#"+frm).prop("checked", true); 
    var frame_id = jQuery("#"+frm).siblings("input").val();
     
  //start
  if(jQuery('#img_preview').attr("src")!=""){
  
  var top=(jQuery('#preview_area').height()-jQuery('#img_preview').height())/2;

  var left=(jQuery('#preview_area').width()-jQuery('#img_preview').width())/2;

  jQuery('#frame_png').attr('style',"top:"+top+"px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px; height:"+jQuery('#img_preview').height()+"px;");
  if(frame_id==1){
       jQuery('#frame_png').attr('src',"<?php echo $this->getSkinUrl('images/empty-frame.png');?>");

    if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
        jQuery('#frame'+id11).val('BLACK FRAME');
    }else{
      jQuery('<input>').attr({
                            type: 'hidden',
                            id: 'frame'+id11,
                            name: 'frame'+id11,
                            value: 'BLACK FRAME'
                        }).appendTo('#hidden_dynamic_fields');
    }

  
  }else if(frame_id==2){
    alert(frame_id);
  
    jQuery('#frame_png').attr('src',"<?php echo $this->getSkinUrl('images/photo_frame.png');?>");
    if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
        jQuery('#frame'+id11).val('BROWN FRAME');
    }else{
  jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'frame'+id11,
                        name: 'frame'+id11,
                        value: 'BROWN FRAME'
                    }).appendTo('#hidden_dynamic_fields');
    }
    
  }else if(frame_id==3){
    jQuery('#frame_png').attr('src',"<?php echo $this->getSkinUrl('images/photo_frame.png');?>");

    if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
    jQuery('#frame'+id11).val('STARCHED CANVAS FRAME');
    }else{
      jQuery('<input>').attr({
                            type: 'hidden',
                            id: 'frame'+id11,
                            name: 'frame'+id11,
                            value: 'STARCHED CANVAS FRAME'
                        }).appendTo('#hidden_dynamic_fields');
    }
        
  }else{
    jQuery('#frame_png').attr('src','');
    jQuery('#frame_png').attr('style','Display:none;');
    if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
        jQuery('#frame'+id11).val('NO FRAME');
    }else{
      jQuery('<input>').attr({
                            type: 'hidden',
                            id: 'frame'+id11,
                            name: 'frame'+id11,
                            value: 'NO FRAME'
                        }).appendTo('#hidden_dynamic_fields');
    }

    }
  }else{
    jQuery(this).removeAttr('checked');
    alert("please select image");
  }
  //end
}



if (size === undefined || size === null && type === undefined || type === null) {
    jQuery(".radio2").prop("checked", false); 
  jQuery('#recomandation_img').attr("src", "<?php echo $this->getSkinUrl('images/images/poor_quality.png');?>");
}else{

 size=size.replace(/\s/g, '');
 size=size.replace('*', '');
  jQuery("#"+size+type).prop("checked", true); 


var contentId = jQuery("#"+size+type).siblings("input[name='size']").val();
  var value=contentId.split("*");
  var height=value[0].trim();
    var width=value[1].trim();
   var newht= (parseInt(height)*96)/11.86;
    var newwdth= (parseInt(width)*96)/11.86;
   jQuery('#preview_area').height(newht);
   jQuery('#preview_area').width(newwdth);
   jQuery('#preview_area').css('margin-top',(350-newht)/2);
var image = new Image();
image.src = jQuery('#img_preview').attr("src");
  var imageht=image.naturalHeight;
  var imagewdt=image.naturalWidth;

// code for recommandation
if(jQuery(this).closest('tr').hasClass('recommended')){
  jQuery('#recomandation_img').attr("src", "<?php echo $this->getSkinUrl('images/images/good_quality.png');?>");
}else{
   jQuery('#recomandation_img').attr("src", "<?php echo $this->getSkinUrl('images/images/poor_quality.png');?>");
}


  if(parseInt(newwdth/newht)>parseInt(imagewdt/imageht)){
    jQuery('#img_preview').attr("style","height:100%;");
    var left=(newwdth-jQuery('#img_preview').width())/2;
    jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");
  }else{
    jQuery('#img_preview').attr("style","width:100%;");
    var margin=(newht-jQuery('#img_preview').height())/2;
    if(margin>0){
      jQuery('#img_preview').attr("style","width:100%;margin-top:"+margin+"px;");
      jQuery('#frame_png').attr("style","left:0px; top:"+margin+"px; width:100%; height:"+jQuery('#img_preview').height()+"px;");
    }else{
      jQuery('#img_preview').attr("style","height:100%;");
      var left=(newwdth-jQuery('#img_preview').width())/2;
      jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");
    }
    
  }

imageht=0;
imagewdt=0;
newwdth=0;
newht=0;
height=0;
width=0;

}
  var object=canvasarr[id1].getActiveObject();
  console.log(canvasarr[id1].getActiveObject());
  var mediumQuality = canvasarr[id1].toDataURL("image/jpeg", 0.5);
  jQuery('#img_preview').attr("src", mediumQuality);

//3dcanvas

if(type=='Rolled'){

  jQuery('#streched_canvas_preview').attr('style','Display:none;');
  jQuery('#rolled_canvas_preview').attr('style','Display:none;');
  jQuery('#gallery_wrap_canvas').attr('style','Display:none;');
  jQuery('#rolled_canvas_preview').removeAttr('style');
  jQuery('#3dpreview').attr('src','mediumQuality');

}else if(type=='Streched'){

  jQuery('#streched_canvas_preview').attr('style','Display:none;');
  jQuery('#rolled_canvas_preview').attr('style','Display:none;');
  jQuery('#gallery_wrap_canvas').attr('style','Display:none;');
  jQuery('#streched_canvas_preview').removeAttr('style');
  jQuery('#3dpreview').attr('style','background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url('+mediumQuality+'); background-size: cover;');

}else{
  jQuery('#streched_canvas_preview').attr('style','Display:none;');
  jQuery('#rolled_canvas_preview').attr('style','Display:none;');
  jQuery('#gallery_wrap_canvas').attr('style','Display:none;');
  jQuery('#gallery_wrap_canvas').removeAttr('style');
  jQuery('#3dpreview').attr('src','mediumQuality');
}

 


  jQuery('#edited_form_img').attr("src", mediumQuality);
var original_img=jQuery('.activeimage').children('img').attr("src");
  jQuery('#selected_form_img').attr("src", original_img);

var image = new Image();
image.src = jQuery('.activeimage').children('img').attr("src");
image.onload = function() {
var image_aspect_ratio=this.width/this.height;
console.log(image_aspect_ratio);

jQuery('#product-review-table tr').removeClass("recommended");

jQuery.each(sizes_obj, function( index, value ) {
  value=value.replace('"', '');
  value=value.replace(/\s/g, ''); 
  value=value.replace('"', '');
  var value = value.split("*");
var isrecommanded=isrecommandedsize(parseInt(value[0]),parseInt(value[1]));
//alert(isrecommanded);
  if( isrecommanded==true){
 
    jQuery('#'+value[0]+value[1]+value[2]).closest('tr').addClass('recommended');

  }
});

//// start 4rth tab

var id11=jQuery('.activeimage').children('img').attr("alt");
 if(jQuery('#media'+id11).length > 0 && jQuery('#media'+id11).val() != ''){
        var typ=jQuery('#media'+id11).val();
         jQuery("#media option[value='"+typ+"']").attr('selected','selected');       
 }
 if(jQuery('#subtype'+id11).length > 0 && jQuery('#subtype'+id11).val() != ''){
        var typ=jQuery('#subtype'+id11).val();
         jQuery("#subtype option[value='"+typ+"']").attr('selected','selected');
     if(typ=='Rolled'){
        var parent=jQuery('#subtype option:selected').val();
            jQuery.ajax({
            type : "POST",
            url : "<?php echo Mage::getBaseUrl();?>mymodule/index/getsize",
            data : {
              parent: parent,
              type: 'Rolled',
              catid: '<?=$_category->getId();?>',
            },
            dataType: "json",
            beforeSend : function() {
                 jQuery('#loader').modal('show');
            },
            success : function(response) {
              var mySelect = jQuery('#size');
              mySelect.html('');
              jQuery.each(response, function(val, text) {
                  mySelect.append(
                      jQuery('<option></option>').val(text.size+' # '+text.price).html('size '+text.size+' price Rs.'+text.price)
                  );
              });
              var siz=jQuery('#size'+id11).val();
              var pric=jQuery('#size_price'+id11).val();
              alert(siz+" # "+pric);
              jQuery("#size option[value='"+siz+" # "+pric+"']").attr('selected','selected');
             jQuery('.price').html('Rs.'+pric);
             jQuery('#price').val(pric);
               jQuery('#loader').modal('hide');
            }
          });
     }else if(typ=='single frame canvas'){
          var frms=jQuery('#frame'+id11).val();
          if(frms=='GALLERY WRAP CANVAS FRAME'){

            var parent=jQuery('#subtype option:selected').val();
            jQuery.ajax({
            type : "POST",
            url : "<?php echo Mage::getBaseUrl();?>mymodule/index/getsize",
            data : {
              parent: parent,
              type: 'Gallery Wrapped Canvas',
              catid: '<?=$_category->getId();?>',
            },
            dataType: "json",
            beforeSend : function() {
                 jQuery('#loader').modal('show');
            },
            success : function(response) {
              var mySelect = jQuery('#size');
              mySelect.html('');
              jQuery.each(response, function(val, text) {
                  mySelect.append(
                      jQuery('<option></option>').val(text.size+' # '+text.price).html('size '+text.size+' price Rs.'+text.price)
                  );
              });
               var siz=jQuery('#size'+id11).val();
              var pric=jQuery('#size_price'+id11).val();
              jQuery("#size option[value='"+siz+" # "+pric+"']").attr('selected','selected');
             jQuery('.price').html('Rs.'+pric);
             jQuery('#price').val(pric);
               jQuery('#loader').modal('hide');
            }
          });


          }else{

            var parent=jQuery('#subtype option:selected').val();
            jQuery.ajax({
            type : "POST",
            url : "<?php echo Mage::getBaseUrl();?>mymodule/index/getsize",
            data : {
              parent: parent,
              type: 'Streched',
              catid: '<?=$_category->getId();?>',
            },
            dataType: "json",
            beforeSend : function() {
                 jQuery('#loader').modal('show');
            },
            success : function(response) {
              var mySelect = jQuery('#size');
              mySelect.html('');
              jQuery.each(response, function(val, text) {
                  mySelect.append(
                      jQuery('<option></option>').val(text.size+' # '+text.price).html('size '+text.size+' price Rs.'+text.price)
                  );
              });
               var siz=jQuery('#size'+id11).val();
              var pric=jQuery('#size_price'+id11).val();
              jQuery("#size option[value='"+siz+" # "+pric+"']").attr('selected','selected');
             jQuery('.price').html('Rs.'+pric);
              jQuery('#price').val(pric);
               jQuery('#loader').modal('hide');
            }
          });


          }
          
     }else{
          var parent=jQuery('#subtype option:selected').val();
            jQuery.ajax({
            type : "POST",
            url : "<?php echo Mage::getBaseUrl();?>mymodule/index/getsize",
            data : {
              parent: parent,
              type: 'Gallery Wrapped Canvas',
              catid: '<?=$_category->getId();?>',
            },
            dataType: "json",
            beforeSend : function() {
                 jQuery('#loader').modal('show');
            },
            success : function(response) {
              var mySelect = jQuery('#size');
              mySelect.html('');
              jQuery.each(response, function(val, text) {
                  mySelect.append(
                      jQuery('<option></option>').val(text.size+' # '+text.price).html('size '+text.size+' price Rs.'+text.price)
                  );
              });
              var siz=jQuery('#size'+id11).val();
              var pric=jQuery('#size_price'+id11).val();
              jQuery("#size option[value='"+siz+" # "+pric+"']").attr('selected','selected');
             jQuery('.price').html('Rs.'+pric);
              jQuery('#price').val(pric);
               jQuery('#loader').modal('hide');
            }
          });
     }
 }
 if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
        var typ=jQuery('#frame'+id11).val();
         jQuery("#frame option[value='"+typ+"']").attr('selected','selected');
 }

  
    if(jQuery('#view'+id11).length > 0 && jQuery('#view'+id11).val() != ''){
        var viw=jQuery('#view'+id11).val();
         jQuery("#view option[value='"+viw+"']").attr('selected','selected');
    }
    if(jQuery('#rotate'+id11).length > 0 && jQuery('#rotate'+id11).val() != ''){
        var rotate=jQuery('#rotate'+id11).val();
       // alert(rotate);
         jQuery("#anglehidden").val(rotate+" degree");
         jQuery("#angle").html(rotate+" degree");
    }
     if(jQuery('#effects'+id11).length > 0 && jQuery('#effects'+id11).val() != ''){
        var eff=jQuery('#effects'+id11).val();
        jQuery("#effect option[value='"+eff+"']").attr('selected','selected');
    }
    if(jQuery('#brightness'+id11).length > 0 && jQuery('#brightness'+id11).val() != ''){
        var bright=jQuery('#brightness'+id11).val();
         jQuery("#brightnesshidden").val(bright);
         jQuery("#brightness").html(bright);
    }
    if(jQuery('#contrast'+id11).length > 0 && jQuery('#contrast'+id11).val() != ''){
        var contras=jQuery('#contrast'+id11).val();
         jQuery("#contrasthidden").val(contras);
         jQuery("#contrast").html(contras);
    }

total= parseInt(jQuery('#price').val())+parseInt(jQuery('#frmprice').val())+parseInt(jQuery('#effprice').val())+parseInt(jQuery('#extraprice').val());

      jQuery('#totalprice').val(total);
      jQuery('.totalprice').html('Rs. '+total);


  


// end 4rd tab activation



if(jQuery('#rotate'+id11).length > 0 && jQuery('#rotate'+id11).val() != ''){
     var res = jQuery('#rotate'+id11).val();
     if(res==0){
      jQuery('#zerorotation').click();
     }else if(res==90){
      jQuery('#ninetyrotation').click();
     }else if(res==180){
      jQuery('#oneeightyrotation').click();
     }else if(res==270){
       jQuery('#twoseventyrotation').click();
     }
  }

};

}else{
    jQuery('#loader').modal('hide');
    alert('Empty frame');
  }

});

//end of thumbnail section

//start of size class


jQuery('.sizeclass').click(function(){

if(jQuery('.activeimage').children('img').attr("src") === undefined || jQuery('.activeimage').children('img').attr("src") === null){

}else{

  jQuery('#loader').modal('show');
   //getting image id
    var id11=jQuery('.activeimage').children('img').attr("alt");
    id1=parseInt(id11)-1;


 

  // create object of canvas active image 
    var object=canvasarr[id1].getActiveObject();

  //creating image of canvas
    var mediumQuality = canvasarr[id1].toDataURL("image/jpeg", 0.5);

  // adding image in step 4
    jQuery('#img_preview').attr("src", mediumQuality);


    
    jQuery('#edited_form_img').attr("src", mediumQuality);

  //adding values to step 4
    var frm=jQuery('#frame'+id11).val();
    var size=jQuery('#size'+id11).val();
    var type=jQuery('#type'+id11).val();

//3d preview
if(type=='Rolled'){

  jQuery('#streched_canvas_preview').attr('style','Display:none;');
  jQuery('#rolled_canvas_preview').attr('style','Display:none;');
  jQuery('#gallery_wrap_canvas').attr('style','Display:none;');
  jQuery('#rolled_canvas_preview').removeAttr('style');
  jQuery('#3dpreview').attr('src','mediumQuality');

}else if(type=='Streched'){

  jQuery('#streched_canvas_preview').attr('style','Display:none;');
  jQuery('#rolled_canvas_preview').attr('style','Display:none;');
  jQuery('#gallery_wrap_canvas').attr('style','Display:none;');
  jQuery('#streched_canvas_preview').removeAttr('style');
  jQuery('#3dpreview').attr('style','background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url('+mediumQuality+'); background-size: cover;');

}else{
  jQuery('#streched_canvas_preview').attr('style','Display:none;');
  jQuery('#rolled_canvas_preview').attr('style','Display:none;');
  jQuery('#gallery_wrap_canvas').attr('style','Display:none;');
  jQuery('#gallery_wrap_canvas').removeAttr('style');
  jQuery('#3dpreview').attr('src','mediumQuality');
}

if(type=='Rolled'||type=='GalleryWrappedCanvas'){
  jQuery('#streched_canvas_frm').removeAttr('style');
  jQuery('#other_frm').removeAttr('style');
  jQuery('#streched_canvas_frm').attr('style','display:none;');
}else{
  jQuery('#streched_canvas_frm').removeAttr('style');
  jQuery('#other_frm').removeAttr('style');
  jQuery('#other_frm').attr('style','display:none;');
}


  //cheking frames addded or not if it is then show respected frame
    jQuery('#frame_png').attr('src','');

    if (frm === undefined || frm === null) {

        jQuery(".radio1").prop("checked", false); 

    }else{

      frm=frm.replace(' ','');
        jQuery("#"+frm).prop("checked", true); 

        var frame_id = jQuery("#"+frm).siblings("input[name='frame']").val();
  
        if(jQuery('#img_preview').attr("src")!=""){


  
            var top=(jQuery('#preview_area').height()-jQuery('#img_preview').height())/2;
            var left=(jQuery('#preview_area').width()-jQuery('#img_preview').width())/2;

            jQuery('#frame_png').attr('style',"top:"+top+"px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px; height:"+jQuery('#img_preview').height()+"px;");

            if(frame_id==1){
                jQuery('#frame_png').attr('src',"<?php echo $this->getSkinUrl('images/empty-frame.png');?>");

                if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
                      jQuery('#frame'+id11).val('BLACK FRAME');
                }else{
                      jQuery('<input>').attr({
                                            type: 'hidden',
                                            id: 'frame'+id11,
                                            name: 'frame'+id11,
                                            value: 'BLACK FRAME'
                                        }).appendTo('#hidden_dynamic_fields');
                }

               
                }else if(frame_id==2){
                    jQuery('#frame_png').attr('src',"<?php echo $this->getSkinUrl('images/photo_frame.png');?>");

                  if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){

                      jQuery('#frame'+id11).val('BROWN FRAME');

                  }else{
                      jQuery('<input>').attr({
                              type: 'hidden',
                              id: 'frame'+id11,
                              name: 'frame'+id11,
                              value: 'BROWN FRAME'
                          }).appendTo('#hidden_dynamic_fields');
                  }

        
                }else if(frame_id==3){
                    
                    jQuery('#frame_png').attr('src',"<?php echo $this->getSkinUrl('images/photo_frame.png');?>");

                  if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
    
                        jQuery('#frame'+id11).val('STARCHED CANVAS FRAME');

                  }else{
                        jQuery('<input>').attr({
                                              type: 'hidden',
                                              id: 'frame'+id11,
                                              name: 'frame'+id11,
                                              value: 'STARCHED CANVAS FRAME'
                                          }).appendTo('#hidden_dynamic_fields');
                  }

        
            }else{

                jQuery('#frame_png').attr('src','');

              if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){

                  jQuery('#frame'+id11).val('NO FRAME');

              }else{

                  jQuery('<input>').attr({
                                        type: 'hidden',
                                        id: 'frame'+id11,
                                        name: 'frame'+id11,
                                        value: 'NO FRAME'
                                    }).appendTo('#hidden_dynamic_fields');

              }

            

            }
   
        }else{
          jQuery(this).removeAttr('checked');
          alert("please select image");
        }
    }



  if (size === undefined || size === null && type === undefined || type === null) {

        jQuery(".radio2").prop("checked", false); 

  }else{


     size=size.replace(/\s/g, '');
     size=size.replace('*', '');
     alert("#"+size+type);
     jQuery("#"+size+type).prop("checked", true); 

     var contentId = jQuery("#"+size+type).siblings("input[name='size']").val();

     var value=contentId.split("*");
     var height=value[0].trim();
     var width=value[1].trim();
     var newht= (parseInt(height)*96)/11.86;
     var newwdth= (parseInt(width)*96)/11.86;
     jQuery('#preview_area').height(newht);
     jQuery('#preview_area').width(newwdth);
     jQuery('#preview_area').css('margin-top',(350-newht)/2);


    var image = new Image();
      image.src = jQuery('#img_preview').attr("src");
        var imageht=image.naturalHeight;
        var imagewdt=image.naturalWidth;



  if(parseInt(newwdth/newht)>parseInt(imagewdt/imageht)){
          jQuery('#img_preview').attr("style","height:100%;");

      var left=(newwdth-jQuery('#img_preview').width())/2;

          jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");

  }else{
          jQuery('#img_preview').attr("style","width:100%;");
      var margin=(newht-jQuery('#img_preview').height())/2;

      if(margin>0){
          jQuery('#img_preview').attr("style","width:100%;margin-top:"+margin+"px;");

          jQuery('#frame_png').attr("style","left:0px; top:"+margin+"px; width:100%; height:"+jQuery('#img_preview').height()+"px;");

      }else{

          jQuery('#img_preview').attr("style","height:100%;");

             var left=(newwdth-jQuery('#img_preview').width())/2;

          jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");

      }
    
  }


imageht=0;
imagewdt=0;
newwdth=0;
newht=0;
height=0;
width=0;
jQuery('.form_size_price').html('Rs. '+value[3]);
jQuery('#form_canvas_size').html(value[0]+' * '+value[1]);
jQuery('#form_canvas_type2').html(value[2]);

}


var image = new Image();
image.src = jQuery('.activeimage').children('img').attr("src");
image.onload = function() {
console.log('height: ' + this.height);
var image_aspect_ratio=this.width/this.height;
console.log(image_aspect_ratio);

jQuery('#loader').modal('hide');

jQuery('#product-review-table tr').removeClass("recommended");

jQuery.each(sizes_obj, function( index, value ) {
  value=value.replace('"', '');
  value=value.replace(/\s/g, ''); 
  value=value.replace('"', '');
  var value = value.split("*");

   var isrecommanded=isrecommandedsize(parseInt(value[0]),parseInt(value[1]));
   //alert(isrecommanded);
  if( isrecommanded==true){
 
    jQuery('#'+value[0]+value[1]+value[2]).closest('tr').addClass('recommended');

  }
});




if(jQuery("#"+size+type).closest('tr').hasClass('recommended')){
  jQuery('#recomandation_img').attr("src", "<?php echo $this->getSkinUrl('images/images/good_quality.png');?>");
}else{
   jQuery('#recomandation_img').attr("src", "<?php echo $this->getSkinUrl('images/images/poor_quality.png');?>");
}


// start

if(jQuery('.radio2').is(':checked')) { 
}else{

  var contentId = '12*12*Rolled*400';
  var value=contentId.split("*");
  var height=value[0].trim();
    var width=value[1].trim();
    var newht= (parseInt(height)*96)/11.86;
    var newwdth= (parseInt(width)*96)/11.86;
   jQuery('#preview_area').height(newht);
   jQuery('#preview_area').width(newwdth);
   jQuery('#preview_area').css('margin-top',(350-newht)/2);
var image = new Image();
image.src = jQuery('#img_preview').attr("src");
  var imageht=image.naturalHeight;
  var imagewdt=image.naturalWidth;

// code for recommandation
if(jQuery(this).closest('tr').hasClass('recommended')){
  jQuery('#recomandation_img').attr("src", "<?php echo $this->getSkinUrl('images/images/good_quality.png');?>");
}else{
   jQuery('#recomandation_img').attr("src", "<?php echo $this->getSkinUrl('images/images/poor_quality.png');?>");
}




  if(parseInt(newwdth/newht)>parseInt(imagewdt/imageht)){
    jQuery('#img_preview').attr("style","height:100%;");
    var left=(newwdth-jQuery('#img_preview').width())/2;
    jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");
  }else{
    jQuery('#img_preview').attr("style","width:100%;");
    var margin=(newht-jQuery('#img_preview').height())/2;
    if(margin>0){
      jQuery('#img_preview').attr("style","width:100%;margin-top:"+margin+"px;");
      jQuery('#frame_png').attr("style","left:0px; top:"+margin+"px; width:100%; height:"+jQuery('#img_preview').height()+"px;");
    }else{
      jQuery('#img_preview').attr("style","height:100%;");
      var left=(newwdth-jQuery('#img_preview').width())/2;
      jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");
    }
    
  }



imageht=0;
imagewdt=0;
newwdth=0;
newht=0;
height=0;
width=0;
//hiiiiiiiiii


if(jQuery('#size'+id11).length > 0 && jQuery('#size'+id11).val() != ''){
    jQuery('#size'+id11).val(value[0]+' * '+value[1]);
  }else{
    jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'size'+id11,
                        name: 'size'+id11,
                        value: value[0]+' * '+value[1]
                    }).appendTo('#hidden_dynamic_fields');
  }
  if(jQuery('#type'+id11).length > 0 && jQuery('#type'+id11).val() != ''){
    jQuery('#type'+id11).val(value[2]);
  }else{
    jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'type'+id11,
                        name: 'type'+id11,
                        value: value[2]
                    }).appendTo('#hidden_dynamic_fields');
  }
  if(jQuery('#size_price'+id11).length > 0 && jQuery('#size_price'+id11).val() != ''){
   jQuery('#size_price'+id11).val(value[3]);
  }else{
    jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'size_price'+id11,
                        name: 'size_price'+id11,
                        value: value[3]
                    }).appendTo('#hidden_dynamic_fields');
  }

if(value[2]=='Rolled'){
  jQuery('#frame_png').attr('style','Display:none;');
      if(jQuery('#subtype'+id11).length > 0 && jQuery('#subtype'+id11).val() != ''){
        jQuery('#subtype'+id11).val('Rolled');

      }else{
       jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'subtype'+id11,
                        name: 'subtype'+id11,
                        value: 'Rolled'
                    }).appendTo('#hidden_dynamic_fields');
      }
    }else{
       if(jQuery('#subtype'+id11).length > 0 && jQuery('#subtype'+id11).val() != ''){
        jQuery('#subtype'+id11).val('single frame canvas');
       if(value[2]=='GalleryWrappedCanvas'){
        jQuery('#frame'+id11).val('GALLERY WRAP CANVAS FRAME');
        jQuery('.radio1').attr('selected',false);
       }else{
          if(jQuery('#frame'+id11).val()=='GALLERY WRAP CANVAS FRAME'){
            jQuery('#frame'+id11).val('NO FRAME');
            jQuery('.radio1').attr('selected',false);
          }
       }


      }else{
       jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'subtype'+id11,
                        name: 'subtype'+id11,
                        value: 'single frame canvas'
                    }).appendTo('#hidden_dynamic_fields');
       if(value[2]=='GalleryWrappedCanvas'){
        jQuery('#frame_png').attr('style','Display:none;');
        jQuery('#frame'+id11).val('GALLERY WRAP CANVAS FRAME');
        jQuery('.radio1').attr('selected',false);
       }else{

          if(jQuery('#frame'+id11).val()=='GALLERY WRAP CANVAS FRAME'){
            jQuery('#frame_png').attr('style','Display:none;');
            jQuery('#frame'+id11).val('NO FRAME');
            jQuery('.radio1').attr('selected',false);
          }
       }

     }
   }















jQuery('.form_size_price').html('Rs. '+value[3]);
jQuery('#form_canvas_size').html(value[0]+' * '+value[1]);
jQuery('#form_canvas_type2').html(value[2]);

jQuery('#recomandation_img').attr('style','Display:inline-block;')
jQuery('#1212Rolled').prop('checked',true);
}
if(jQuery('#1212Rolled').closest('tr').hasClass('recommended')){
  jQuery('#recomandation_img').attr("src", "<?php echo $this->getSkinUrl('images/images/good_quality.png');?>");
}else{
   jQuery('#recomandation_img').attr("src", "<?php echo $this->getSkinUrl('images/images/poor_quality.png');?>");
}

//end
};

}

});




jQuery('#recomandation_img').attr('style','Display:none;');
// code for size show


// radio click start 

jQuery(".radio2").change(function () {
  
  jQuery('#loader').modal('show');
  var contentId = jQuery(this).siblings("input[name='size']").val();
  var value=contentId.split("*");
  var height=value[0].trim();
    var width=value[1].trim();
    var newht= (parseInt(height)*96)/11.86;
    var newwdth= (parseInt(width)*96)/11.86;
   jQuery('#preview_area').height(newht);
   jQuery('#preview_area').width(newwdth);
   jQuery('#preview_area').css('margin-top',(350-newht)/2);
var image = new Image();
image.src = jQuery('#img_preview').attr("src");
  var imageht=image.naturalHeight;
  var imagewdt=image.naturalWidth;

// code for recommandation
if(jQuery(this).closest('tr').hasClass('recommended')){
  jQuery('#recomandation_img').attr("src", "<?php echo $this->getSkinUrl('images/images/good_quality.png');?>");
}else{
   jQuery('#recomandation_img').attr("src", "<?php echo $this->getSkinUrl('images/images/poor_quality.png');?>");
}




  if(parseInt(newwdth/newht)>parseInt(imagewdt/imageht)){
    jQuery('#img_preview').attr("style","height:100%;");
    var left=(newwdth-jQuery('#img_preview').width())/2;
    jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");
  }else{
    jQuery('#img_preview').attr("style","width:100%;");
    var margin=(newht-jQuery('#img_preview').height())/2;
    if(margin>0){
      jQuery('#img_preview').attr("style","width:100%;margin-top:"+margin+"px;");
      jQuery('#frame_png').attr("style","left:0px; top:"+margin+"px; width:100%; height:"+jQuery('#img_preview').height()+"px;");
    }else{
      jQuery('#img_preview').attr("style","height:100%;");
      var left=(newwdth-jQuery('#img_preview').width())/2;
      jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");
    }
    
  }

imageht=0;
imagewdt=0;
newwdth=0;
newht=0;
height=0;
width=0;

var id11=jQuery('.activeimage').children('img').attr("alt");
   var id1=parseInt(id11)-1;

if(value[2]=='Rolled'||value[2]=='GalleryWrappedCanvas'){
  jQuery('#streched_canvas_frm').removeAttr('style');
  jQuery('#other_frm').removeAttr('style');
  jQuery('#streched_canvas_frm').attr('style','display:none;');
}else{
  jQuery('#streched_canvas_frm').removeAttr('style');
  jQuery('#other_frm').removeAttr('style');
  jQuery('#other_frm').attr('style','display:none;');
}

if(jQuery('#size'+id11).length > 0 && jQuery('#size'+id11).val() != ''){
    jQuery('#size'+id11).val(value[0]+' * '+value[1]);
  }else{
    jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'size'+id11,
                        name: 'size'+id11,
                        value: value[0]+' * '+value[1]
                    }).appendTo('#hidden_dynamic_fields');
  }
  if(jQuery('#type'+id11).length > 0 && jQuery('#type'+id11).val() != ''){
    jQuery('#type'+id11).val(value[2]);
  }else{
    jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'type'+id11,
                        name: 'type'+id11,
                        value: value[2]
                    }).appendTo('#hidden_dynamic_fields');
  }

    var subtp=jQuery('#subtype'+id11).val();
    
    var finalpriceincmt=0;
    if(subtp=='three frame canvas'){
      finalpriceincmt=value[3]*(0.1);
    }else if(subtp=='five frame canvas'){
      finalpriceincmt=value[3]*(0.3);
    }else{
      finalpriceincmt=0;
    }

  if(jQuery('#size_price'+id11).length > 0 && jQuery('#size_price'+id11).val() != ''){
   jQuery('#size_price'+id11).val(parseFloat(value[3])+parseFloat(finalpriceincmt));
  }else{
    jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'size_price'+id11,
                        name: 'size_price'+id11,
                        value: parseFloat(value[3])+parseFloat(finalpriceincmt)
                    }).appendTo('#hidden_dynamic_fields');
  }
  if(value[2]=='Rolled'){
    jQuery('#frame_png').attr('style','Display:none;');
    jQuery('.radio1').attr('selected',false);
      if(jQuery('#subtype'+id11).length > 0 && jQuery('#subtype'+id11).val() != ''){
        jQuery('#subtype'+id11).val('Rolled');

      }else{
       jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'subtype'+id11,
                        name: 'subtype'+id11,
                        value: 'Rolled'
                    }).appendTo('#hidden_dynamic_fields');
      }
      if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
        jQuery('#frame'+id11).val('NO FRAME');

      }else{
       jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'frame'+id11,
                        name: 'frame'+id11,
                        value: 'NO FRAME'
                    }).appendTo('#hidden_dynamic_fields');
      }
    }else{
       if(jQuery('#subtype'+id11).length > 0 && jQuery('#subtype'+id11).val() != ''){
        if(jQuery('#subtype'+id11).val()=='Rolled'){
          jQuery('#subtype'+id11).val('single frame canvas');
        } 
       if(value[2]=='GalleryWrappedCanvas'){
        jQuery('#frame_png').attr('style','Display:none;');
        jQuery('#frame'+id11).val('GALLERY WRAP CANVAS FRAME');
        jQuery('.radio1').attr('selected',false);
       }else{
          if(jQuery('#frame'+id11).val()=='GALLERY WRAP CANVAS FRAME'){
            jQuery('#frame_png').attr('style','Display:none;');
            jQuery('.radio1').attr('selected',false);
            jQuery('#frame'+id11).val('NO FRAME');
          }
       }


      }else{
       jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'subtype'+id11,
                        name: 'subtype'+id11,
                        value: 'single frame canvas'
                    }).appendTo('#hidden_dynamic_fields');
       if(value[2]=='GalleryWrappedCanvas'){
        jQuery('#frame'+id11).val('GALLERY WRAP CANVAS FRAME');
        jQuery('.radio1').attr('selected',false);
       }else{
          if(jQuery('#frame'+id11).val()=='GALLERY WRAP CANVAS FRAME'){
            jQuery('#frame'+id11).val('NO FRAME');
            jQuery('.radio1').attr('selected',false);
          }
       }

     }
   }
jQuery('#recomandation_img').attr('style','Display:inline-block;')


//3d preview
if(value[2]=='Rolled'){

  jQuery('#streched_canvas_preview').attr('style','Display:none;');
  jQuery('#rolled_canvas_preview').attr('style','Display:none;');
  jQuery('#gallery_wrap_canvas').attr('style','Display:none;');
  jQuery('#rolled_canvas_preview').removeAttr('style');

}else if(value[2]=='Streched'){

  jQuery('#streched_canvas_preview').attr('style','Display:none;');
  jQuery('#rolled_canvas_preview').attr('style','Display:none;');
  jQuery('#gallery_wrap_canvas').attr('style','Display:none;');
  jQuery('#streched_canvas_preview').removeAttr('style');

  jQuery('#3dpreview').attr('style','background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url('+jQuery('#img_preview').attr("src")+'); background-size: cover;');

}else{
  jQuery('#streched_canvas_preview').attr('style','Display:none;');
  jQuery('#rolled_canvas_preview').attr('style','Display:none;');
  jQuery('#gallery_wrap_canvas').attr('style','Display:none;');
  jQuery('#gallery_wrap_canvas').removeAttr('style');
}

jQuery('#loader').modal('hide');


  });

// radio click ends

//frame radio click event




jQuery(".radio1").on('change',function () {
  if(jQuery('#img_preview').attr("src")!=""){
var id11=jQuery('.activeimage').children('img').attr("alt");
   var id1=parseInt(id11)-1;
 
  var top=(jQuery('#preview_area').height()-jQuery('#img_preview').height())/2;
  var left=(jQuery('#preview_area').width()-jQuery('#img_preview').width())/2;
  jQuery('#frame_png').attr('style',"top:"+top+"px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px; height:"+jQuery('#img_preview').height()+"px;");
   var frame_id = jQuery(this).siblings("input[name='frame']").val();
    if(frame_id==1){
       jQuery('#frame_png').attr('src',"<?php echo $this->getSkinUrl('images/empty-frame.png');?>");

if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
    jQuery('#frame'+id11).val('BLACK FRAME');
}else{
  jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'frame'+id11,
                        name: 'frame'+id11,
                        value: 'BLACK FRAME'
                    }).appendTo('#hidden_dynamic_fields');
}


     }else if(frame_id==2){
       jQuery('#frame_png').attr('src',"<?php echo $this->getSkinUrl('images/photo_frame.png');?>");

if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
    jQuery('#frame'+id11).val('BROWN FRAME');
}else{
  jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'frame'+id11,
                        name: 'frame'+id11,
                        value: 'BROWN FRAME'
                    }).appendTo('#hidden_dynamic_fields');
}


     }else if(frame_id==3){
        jQuery('#frame_png').attr('src',"<?php echo $this->getSkinUrl('images/photo_frame.png');?>");

if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
    jQuery('#frame'+id11).val('STARCHED CANVAS FRAME');
}else{
  jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'frame'+id11,
                        name: 'frame'+id11,
                        value: 'STARCHED CANVAS FRAME'
                    }).appendTo('#hidden_dynamic_fields');
}

jQuery('#form_canvas_frame').html('strached_canvas_frame');

     }else{
        jQuery('#frame_png').attr('src','');

if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
    jQuery('#frame'+id11).val('NO FRAME');
}else{
  jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'frame'+id11,
                        name: 'frame'+id11,
                        value: 'NO FRAME'
                    }).appendTo('#hidden_dynamic_fields');
}

jQuery('#form_canvas_frame').html('NO FRAME');


     }
   
  }else{
    jQuery(this).removeAttr('checked');
    alert("please select image");
  }
  
});
//end of frm click event

//start to change to original image


jQuery('#original').click(function(){


jQuery('#loader').modal('show');

  var id11=jQuery('.activeimage').children('img').attr("alt");
  var original_src=jQuery('.activeimage').children('img').attr("src");
  var id1=parseInt(id11)-1;
  var object=canvasarr[id1].getActiveObject();

  canvasarr[id1].remove(canvasarr[id1].getActiveObject());
   var imgObj = new Image();
        imgObj.src = original_src;
        imgObj.onload = function () {
            // start fabricJS stuff
            var image = new fabric.Image(imgObj);
             image.set({
                    left: 0,
                    top: 0, 
                });
             
             canvasarr[id1].height=image.height;
             canvasarr[id1].width=image.width;

            canvasarr[id1].add(image);
            canvasarr[id1].setActiveObject(canvas.item(0));
             var object=canvas.getActiveObject();
            var filter = new fabric.Image.filters.Brightness({brightness:0
            });
            object.filters.push(filter);
            object.applyFilters(canvas.renderAll.bind(canvas));
            var filter1 = new fabric.Image.filters.Tint({
              color: '#000000',
              opacity: 0.0
            });
            object.filters.push(filter1);
            object.applyFilters(canvas.renderAll.bind(canvas));
            
            jQuery('#process_img_temp_view').attr('src', jQuery('.activeimage').children('img').attr("src"));
          }


          jQuery('#loader').modal('hide');
});

//end to original image


// portrait view 


jQuery('#portrait_view').click(function(){
 
  var contentId = jQuery("input[name='heightwidthroll']:checked").siblings("input[name='size']").val();
  var value=contentId.split("*");
  var width=value[0].trim();
    var height=value[1].trim();
    var newht= (parseInt(height)*96)/11.86;
    var newwdth= (parseInt(width)*96)/11.86;
   jQuery('#preview_area').height(newht);
   jQuery('#preview_area').width(newwdth);

   if(((350-newht)/2)>0){
    jQuery('#preview_area').css('margin-top',(350-newht)/2);
  }else{
    jQuery('#preview_area').css('margin-top','5px');
  }
   
var image = new Image();
image.src = jQuery('#img_preview').attr("src");
  var imageht=image.naturalHeight;
  var imagewdt=image.naturalWidth;
  if(parseInt(newwdth/newht)>parseInt(imagewdt/imageht)){
    jQuery('#img_preview').attr("style","height:100%;");
    var left=(newwdth-jQuery('#img_preview').width())/2;
    jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");
  }else{
    jQuery('#img_preview').attr("style","width:100%;");
    var margin=(newht-jQuery('#img_preview').height())/2;
    if(margin>0){
      jQuery('#img_preview').attr("style","width:100%;margin-top:"+margin+"px;");
      jQuery('#frame_png').attr("style","left:0px; top:"+margin+"px; width:100%; height:"+jQuery('#img_preview').height()+"px;");
    }else{
      jQuery('#img_preview').attr("style","height:100%;");
      var left=(newwdth-jQuery('#img_preview').width())/2;
      jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");
    }
    
  }

imageht=0;
imagewdt=0;
newwdth=0;
newht=0;
height=0;
width=0;

var id11=jQuery('.activeimage').children('img').attr("alt");
   var id1=parseInt(id11)-1;
if(jQuery('#view'+id11).length > 0 && jQuery('#view'+id11).val() != ''){
    jQuery('#view'+id11).val('PORTRAIT');
}else{
  jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'view'+id11,
                        name: 'view'+id11,
                        value: 'PORTRAIT'
                    }).appendTo('#hidden_dynamic_fields');
}

});

//end of portrait view

//start of landscape view 



jQuery('#Landscape_view').click(function(){

  var contentId = jQuery("input[name='heightwidthroll']:checked").siblings("input[name='size']").val();
  var value=contentId.split("*");
  var height=value[0].trim();
    var width=value[1].trim();
    var newht= (parseInt(height)*96)/11.86;
    var newwdth= (parseInt(width)*96)/11.86;
   jQuery('#preview_area').height(newht);
   jQuery('#preview_area').width(newwdth);
   jQuery('#preview_area').css('margin-top',(350-newht)/2);
var image = new Image();
image.src = jQuery('#img_preview').attr("src");
  var imageht=image.naturalHeight;
  var imagewdt=image.naturalWidth;
  if(parseInt(newwdth/newht)>parseInt(imagewdt/imageht)){
    jQuery('#img_preview').attr("style","height:100%;");
    var left=(newwdth-jQuery('#img_preview').width())/2;
    jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");
  }else{
    jQuery('#img_preview').attr("style","width:100%;");
    var margin=(newht-jQuery('#img_preview').height())/2;
    if(margin>0){
      jQuery('#img_preview').attr("style","width:100%;margin-top:"+margin+"px;");
      jQuery('#frame_png').attr("style","left:0px; top:"+margin+"px; width:100%; height:"+jQuery('#img_preview').height()+"px;");
    }else{
      jQuery('#img_preview').attr("style","height:100%;");
      var left=(newwdth-jQuery('#img_preview').width())/2;
      jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");
    }
    
  }

imageht=0;
imagewdt=0;
newwdth=0;
newht=0;
height=0;
width=0;

var id11=jQuery('.activeimage').children('img').attr("alt");
   var id1=parseInt(id11)-1;
if(jQuery('#view'+id11).length > 0 && jQuery('#view'+id11).val() != ''){
    jQuery('#view'+id11).val('LANDSCAPE');
}else{
  jQuery('<input>').attr({
                        type: 'hidden',
                        id: 'view'+id11,
                        name: 'view'+id11,
                        value: 'LANDSCAPE'
                    }).appendTo('#hidden_dynamic_fields');
}

});


// end of landscape view


// get recommandation size function
function getrecommandedpixel(size){
  switch(size){
    case 12:{
      return 600;
    }
    case 18:{
      return 900;
    }
    case 24:{
      return 1200;
    }
    case 30:{
      return 1800;
    }
    case 36:{
      return 2200;
    }
    case 42:{
      return 2400;
    }
    case 48:{
      return 2800;
    }
    case 60:{
      return 3200;
    }
    case 72:{
      return 3600;
    }
    default:{
      return 0;
    }
  }
}

// end of recommandation size function

//crop btn click 



        jQuery('#cropB').click(function () {

         
            jQuery('#process_img_temp_view').selectAreas({
            minSize: [10, 10],
            onChanged : debugQtyAreas,
            width: 500,
            });

            jQuery('.actionOff').attr("disabled", "disabled");
            jQuery('.actionOn').removeAttr("disabled");
           
         

            var areas = jQuery('#process_img_temp_view').selectAreas('relativeAreas');
            var height_d,width_d,x_d,y_d;

            var src_d=jQuery('#process_img_temp_view').attr('src');
            jQuery.each(areas, function (id, area) {
              height_d=area.height;
              width_d=area.width;
              x_d=area.x;
              y_d=area.y;
            });

            if( (height_d === undefined || height_d === null) && (width_d === undefined || width_d === null) && 
              (x_d === undefined || x_d === null) && 
              (y_d === undefined || y_d === null) ){
             //alert('please select area first!!!');
            }else{
              var image=new Image();
              image.src=src_d;
              var htratio=(image.height/jQuery('.select-areas-overlay').height());
              var wdtratio=(image.width/jQuery('.select-areas-overlay').width());

              console.log(height_d*wdtratio);
              console.log(width_d*htratio);
              console.log(x_d*wdtratio);
              console.log(y_d*htratio);
              console.log(src_d);
              jQuery('#process_img_temp_view').selectAreas('reset');


              jQuery.ajax({
                    type : "POST",
                    url : "<?php echo Mage::getBaseUrl();?>mymodule/index/imagecrop",
                    data : {
                      x: x_d*wdtratio,
                      y: y_d*htratio,
                      w: width_d*htratio,
                      h: height_d*wdtratio,
                      img: src_d
                    },
                    beforeSend : function() {
                         jQuery('#loader').modal('show');
                    },
                    success : function(response) {
                      console.log(response);
                    

            var id11=jQuery('.activeimage').children('img').attr("alt");
            var id1=parseInt(id11)-1;


            var object=canvasarr[id1].getActiveObject();

            canvasarr[id1].remove(canvasarr[id1].getActiveObject());

             var imgObj = new Image();
                  imgObj.src = response;
                  imgObj.onload = function () {
                      // start fabricJS stuff
                      
                      var image = new fabric.Image(imgObj);
                       image.set({
                              left: 0,
                              top: 0, 
                          });
                       canvasarr[id1].height=image.height;
                       canvasarr[id1].width=image.width;
                      canvas.add(image);
                      canvas.setActiveObject(canvas.item(0));
                       var object=canvas.getActiveObject();
                      var filter = new fabric.Image.filters.Brightness({brightness:0
                      });
                      object.filters.push(filter);
                      object.applyFilters(canvas.renderAll.bind(canvas));
                      var filter1 = new fabric.Image.filters.Tint({
                        color: '#000000',
                        opacity: 0.0
                      });
                      object.filters.push(filter1);
                      object.applyFilters(canvas.renderAll.bind(canvas));
                    }


                    jQuery('#process_img_temp_view').attr('src',response);
                      jQuery('#process_img_temp_view').selectAreas({
                      minSize: [10, 10],
                      onChanged : debugQtyAreas,
                      width: 500,
                      });


                      jQuery('#loader').modal('hide');

                    }
                });

            }            

        });

//crop btn end

//deselect area

        jQuery('.delete-area').click(function(){
          isselectactivated=false;
          alert();
          jQuery('#process_img_temp_view').selectAreas('destroy');
        });

//rotate click
jQuery('.rotate').click(function(){
 
  var id11=jQuery('.activeimage').children('img').attr("alt");
  var alt=jQuery(this).attr('alt');
  if(alt==1){
    jQuery('#process_img_temp_view').removeClass('rotate180deg');
    jQuery('#process_img_temp_view').removeClass('rotate270deg');
    jQuery('#process_img_temp_view').removeClass('rotate90deg');
    jQuery('#edited_form_img').removeClass('editrotate180deg');
    jQuery('#edited_form_img').removeClass('editrotate270deg');
    jQuery('#edited_form_img').removeClass('editrotate90deg');
    jQuery('#process_img_temp_view').addClass('rotate90deg');
    jQuery('#edited_form_img').addClass('editrotate90deg');


    if(jQuery('#rotate'+id11).length > 0 && jQuery('#rotate'+id11).val() != ''){
        jQuery('#rotate'+id11).val(90);
      }else{
            jQuery('<input>').attr({
              type: 'hidden',
              id: 'rotate'+id11,
              name: 'rotate'+id11,
              value: 90
          }).appendTo('#hidden_dynamic_fields');
      }


  }else if(alt==2){
    jQuery('#process_img_temp_view').removeClass('rotate180deg');
    jQuery('#process_img_temp_view').removeClass('rotate270deg');
    jQuery('#process_img_temp_view').removeClass('rotate90deg');
    jQuery('#edited_form_img').removeClass('editrotate180deg');
    jQuery('#edited_form_img').removeClass('editrotate270deg');
    jQuery('#edited_form_img').removeClass('editrotate90deg');
    jQuery('#process_img_temp_view').addClass('rotate180deg');
    jQuery('#edited_form_img').addClass('editrotate180deg');

     if(jQuery('#rotate'+id11).length > 0 && jQuery('#rotate'+id11).val() != ''){
        jQuery('#rotate'+id11).val(180);
      }else{
            jQuery('<input>').attr({
              type: 'hidden',
              id: 'rotate'+id11,
              name: 'rotate'+id11,
              value: 180
          }).appendTo('#hidden_dynamic_fields');
      }

  }else if(alt==3){
    jQuery('#process_img_temp_view').removeClass('rotate180deg');
    jQuery('#process_img_temp_view').removeClass('rotate270deg');
    jQuery('#process_img_temp_view').removeClass('rotate90deg');
    jQuery('#edited_form_img').removeClass('editrotate180deg');
    jQuery('#edited_form_img').removeClass('editrotate270deg');
    jQuery('#edited_form_img').removeClass('editrotate90deg');
    jQuery('#process_img_temp_view').addClass('rotate270deg');
    jQuery('#edited_form_img').addClass('editrotate270deg');

     if(jQuery('#rotate'+id11).length > 0 && jQuery('#rotate'+id11).val() != ''){
        jQuery('#rotate'+id11).val(270);
      }else{
            jQuery('<input>').attr({
              type: 'hidden',
              id: 'rotate'+id11,
              name: 'rotate'+id11,
              value: 270
          }).appendTo('#hidden_dynamic_fields');
      }

  }else{
    jQuery('#process_img_temp_view').removeClass('rotate180deg');
    jQuery('#process_img_temp_view').removeClass('rotate270deg');
    jQuery('#process_img_temp_view').removeClass('rotate90deg');
    jQuery('#edited_form_img').removeClass('editrotate180deg');
    jQuery('#edited_form_img').removeClass('editrotate270deg');
    jQuery('#edited_form_img').removeClass('editrotate90deg');

     if(jQuery('#rotate'+id11).length > 0 && jQuery('#rotate'+id11).val() != ''){
        jQuery('#rotate'+id11).val(0);
      }else{
            jQuery('<input>').attr({
              type: 'hidden',
              id: 'rotate'+id11,
              name: 'rotate'+id11,
              value: 0
          }).appendTo('#hidden_dynamic_fields');
      }

  }
});


// 3d preview hover


 
function openModalPopupWindow() {
    jQuery( "#preview" ).modal( "show" );
} 

jQuery("#preview_area img").hover(function() {
    openModalPopupWindow();
}, 
function() {
   
});

//end of hover


// start of correction tab 



jQuery('.corrections_tab').click(function(){

var id11=jQuery('.activeimage').children('img').attr("alt");
 if(jQuery('#media'+id11).length > 0 && jQuery('#media'+id11).val() != ''){
        var typ=jQuery('#media'+id11).val();
         jQuery("#media option[value='"+typ+"']").attr('selected','selected');       
 }
 if(jQuery('#subtype'+id11).length > 0 && jQuery('#subtype'+id11).val() != ''){
        var typ=jQuery('#subtype'+id11).val();
         jQuery("#subtype option[value='"+typ+"']").attr('selected','selected');
     if(typ=='Rolled'){

      jQuery('#frame').html('');
     jQuery('#frame').append(jQuery('<option>', { 
        value: 'NO FRAME',
        text : 'NO FRAME' 
    }));
 jQuery("#frame option[value='NO FRAME']").attr('selected',true);



        var parent=jQuery('#subtype option:selected').val();
            jQuery.ajax({
            type : "POST",
            url : "<?php echo Mage::getBaseUrl();?>mymodule/index/getsize",
            data : {
              parent: parent,
              type: 'Rolled',
              catid: '<?=$_category->getId();?>',
            },
            dataType: "json",
            beforeSend : function() {
                 jQuery('#loader').modal('show');
            },
            success : function(response) {
              var mySelect = jQuery('#size');
              mySelect.html('');
              jQuery.each(response, function(val, text) {
                  mySelect.append(
                      jQuery('<option></option>').val(text.size+' # '+text.price).html('size '+text.size+' price Rs.'+text.price)
                  );
              });
              var siz=jQuery('#size'+id11).val();
              var pric=jQuery('#size_price'+id11).val();
              alert(siz+" # "+pric);
              jQuery("#size option[value='"+siz+" # "+pric+"']").attr('selected','selected');
             jQuery('.price').html('Rs.'+pric);
              jQuery('#price').val(pric);
               jQuery('#loader').modal('hide');
            }
          });
     }else if(typ=='single frame canvas'){
          var frms=jQuery('#frame'+id11).val();
          
    jQuery('#frame').html('');
     jQuery('#frame').append(jQuery('<option>', { 
        value: 'NO FRAME',
        text : 'NO FRAME' 
    }));
     jQuery('#frame').append(jQuery('<option>', { 
        value: 'BLACK FRAME',
        text : 'BLACK FRAME' 
    }));
     jQuery('#frame').append(jQuery('<option>', { 
        value: 'BROWN FRAME',
        text : 'BROWN FRAME' 
    }));
     jQuery('#frame').append(jQuery('<option>', { 
        value: 'STARCHED CANVAS FRAME',
        text : 'STARCHED CANVAS FRAME' 
    }));
     jQuery('#frame').append(jQuery('<option>', { 
        value: 'GALLERY WRAP CANVAS FRAME',
        text : 'GALLERY WRAP CANVAS FRAME' 
    }));
jQuery("#frame option[value='"+frms+"']").attr('selected',true);

          if(frms=='GALLERY WRAP CANVAS FRAME'){

  
            var parent=jQuery('#subtype option:selected').val();
            jQuery.ajax({
            type : "POST",
            url : "<?php echo Mage::getBaseUrl();?>mymodule/index/getsize",
            data : {
              parent: parent,
              type: 'Gallery Wrapped Canvas',
              catid: '<?=$_category->getId();?>',
            },
            dataType: "json",
            beforeSend : function() {
                 jQuery('#loader').modal('show');
            },
            success : function(response) {
              var mySelect = jQuery('#size');
              mySelect.html('');
              jQuery.each(response, function(val, text) {
                  mySelect.append(
                      jQuery('<option></option>').val(text.size+' # '+text.price).html('size '+text.size+' price Rs.'+text.price)
                  );
              });
               var siz=jQuery('#size'+id11).val();
              var pric=jQuery('#size_price'+id11).val();
              jQuery("#size option[value='"+siz+" # "+pric+"']").attr('selected','selected');
             jQuery('.price').html('Rs.'+pric);
              jQuery('#price').val(pric);
               jQuery('#loader').modal('hide');
            }
          });


          }else{

            var parent=jQuery('#subtype option:selected').val();
            jQuery.ajax({
            type : "POST",
            url : "<?php echo Mage::getBaseUrl();?>mymodule/index/getsize",
            data : {
              parent: parent,
              type: 'Streched',
              catid: '<?=$_category->getId();?>',
            },
            dataType: "json",
            beforeSend : function() {
                 jQuery('#loader').modal('show');
            },
            success : function(response) {
              var mySelect = jQuery('#size');
              mySelect.html('');
              jQuery.each(response, function(val, text) {
                  mySelect.append(
                      jQuery('<option></option>').val(text.size+' # '+text.price).html('size '+text.size+' price Rs.'+text.price)
                  );
              });
               var siz=jQuery('#size'+id11).val();
              var pric=jQuery('#size_price'+id11).val();
              jQuery("#size option[value='"+siz+" # "+pric+"']").attr('selected','selected');
             jQuery('.price').html('Rs.'+pric);
              jQuery('#price').val(pric);

               jQuery('#loader').modal('hide');
            }
          });


          }
          
     }else{
          var parent=jQuery('#subtype option:selected').val();
            jQuery.ajax({
            type : "POST",
            url : "<?php echo Mage::getBaseUrl();?>mymodule/index/getsize",
            data : {
              parent: parent,
              type: 'Gallery Wrapped Canvas',
              catid: '<?=$_category->getId();?>',
            },
            dataType: "json",
            beforeSend : function() {
                 jQuery('#loader').modal('show');
            },
            success : function(response) {
              var mySelect = jQuery('#size');
              mySelect.html('');
              jQuery.each(response, function(val, text) {
                  mySelect.append(
                      jQuery('<option></option>').val(text.size+' # '+text.price).html('size '+text.size+' price Rs.'+text.price)
                  );
              });
              var siz=jQuery('#size'+id11).val();
              var pric=jQuery('#size_price'+id11).val();
              jQuery("#size option[value='"+siz+" # "+pric+"']").attr('selected','selected');
             jQuery('.price').html('Rs.'+pric);
              jQuery('#price').val(pric);
               jQuery('#loader').modal('hide');
            }
          });
     }
 }
 if(jQuery('#frame'+id11).length > 0 && jQuery('#frame'+id11).val() != ''){
        var typ=jQuery('#frame'+id11).val();
         jQuery("#frame option[value='"+typ+"']").attr('selected','selected');
 }

  
    if(jQuery('#view'+id11).length > 0 && jQuery('#view'+id11).val() != ''){
        var viw=jQuery('#view'+id11).val();
         jQuery("#view option[value='"+viw+"']").attr('selected','selected');
    }
    if(jQuery('#rotate'+id11).length > 0 && jQuery('#rotate'+id11).val() != ''){
        var rotate=jQuery('#rotate'+id11).val();
       // alert(rotate);
         jQuery("#anglehidden").val(rotate+" degree");
         jQuery("#angle").html(rotate+" degree");
    }
     if(jQuery('#effects'+id11).length > 0 && jQuery('#effects'+id11).val() != ''){
        var eff=jQuery('#effects'+id11).val();
        jQuery("#effect option[value='"+eff+"']").attr('selected','selected');
    }
    if(jQuery('#brightness'+id11).length > 0 && jQuery('#brightness'+id11).val() != ''){
        var bright=jQuery('#brightness'+id11).val();
         jQuery("#brightnesshidden").val(bright);
         jQuery("#brightness").html(bright);
    }
    if(jQuery('#contrast'+id11).length > 0 && jQuery('#contrast'+id11).val() != ''){
        var contras=jQuery('#contrast'+id11).val();
         jQuery("#contrasthidden").val(contras);
         jQuery("#contrast").html(contras);
    }

total= parseInt(jQuery('#price').val())+parseInt(jQuery('#frmprice').val())+parseInt(jQuery('#effprice').val())+parseInt(jQuery('#extraprice').val());

      jQuery('#totalprice').val(total);
      jQuery('.totalprice').html('Rs. '+total);

});

//end of correction tab

//start of recommended size



function isrecommandedsize(height,width) {

var image=new Image();
image.src=jQuery('.activeimage').children('img').attr("src");

var aspect_ratio=width/height;

var asp_ratio=aspect_ratio*0.35;

var img_aspect_ratio=image.width/image.height;

    if((aspect_ratio+0.2 > img_aspect_ratio) && (aspect_ratio-0.2 < img_aspect_ratio)){
     
      htdiff=getrecommandedpixel(parseInt(height))*0.10;
      wdthdiff=getrecommandedpixel(parseInt(width))*0.10;
     
          if((((getrecommandedpixel(parseInt(height))-htdiff)<image.height)) && (((getrecommandedpixel(parseInt(width))-wdthdiff)<image.width))){

            return true;

          }else{

            return false;

          }

    }else{
      return false;
    }


}

//end of recommandation size

// add to cart start 

jQuery('#addtocart').click(function(){

var id1=jQuery('.activeimage').children('img').attr("alt");
 
   if(id1.length > 0 && id1 != ''){
   // jQuery('#loader').modal('show');

    //start
    //default fields
    var category_id= jQuery("input[name='category_id']").val();
    var order_type= jQuery("input[name='order_type']").val();
    var type= jQuery("input[name='type"+id1+"']").val();


    //image data 
    var original_img=jQuery('.activeimage').children('img').attr("src");
    var edited_img=jQuery("#edited_form_img").attr('src');


    //4rth tab
    var canvas_type= jQuery("#subtype option:selected").val();
    var media= jQuery("#media option:selected").val();
    var frame= jQuery("#frame option:selected").val();

    var sizeselected=jQuery("#size option:selected").val();
        sizeselected = sizeselected.split('#');

        var order_size= sizeselected[0].trim();
        

    var view= jQuery("#view option:selected").val();
    var effects= jQuery("#effect option:selected").val();
    var rotation= jQuery("#anglehidden").val();
    var brightness= jQuery("#brightnesshidden").val();
    var contrast=jQuery("#contrasthidden").val();

    var prj_name= jQuery("input[name='prj_name']").val();
    var prj_comments= jQuery("textarea[name='prj_comments']").val();


    var digitalcorrections= jQuery("#digitalcorrections").val();
    var major_retouching=' ';
    var minor_change_array=' ';

    if(digitalcorrections=='Minor Retouching'){
      var matches = [];
      jQuery(".radio5:checked").each(function() {
          matches.push(this.value);
          alert(this.value);
      });
      minor_change_array=matches;
    }
    if(digitalcorrections=='Major Retouching'){
      major_retouching=jQuery('#majorretouch').val();
    }
    
  
    // price things
    var three_five_frm_cost=0;
    var price1= jQuery("#price").val();
    var price=0;
var totalpric=jQuery("#price").val();
    if(frame=='GALLERY WRAP CANVAS FRAME'){
        if(canvas_type=='three frame canvas'){
          three_five_frm_cost=price1*0.1;
          price=price1-three_five_frm_cost;
        }
        if(canvas_type=='five frame canvas'){
          three_five_frm_cost=price1*0.3;
          price=price1-three_five_frm_cost;
        }
    }

    var effects_cost=0;
var size_price= price;





    if("<?php echo Mage::getSingleton('customer/session')->isLoggedIn();?>" == 'false'){
        //not logged in ?>

jQuery('#loginmdl').modal('show');



 }else{

            jQuery.ajax({
                  type : "POST",
                  url : "<?php echo Mage::getBaseUrl();?>mymodule/index/imageupload",
                    dataType: 'json',
                    data : {
                      category_id: category_id,
                      order_type: order_type,
                      canvas_type: canvas_type,
                      media: media,
                      type: type,
                      type: type,
                      size: order_size,
                      size_price: size_price,
                      frame: frame,
                      view: view,
                      effects: effects,
                      rotation: rotation,
                      brightness: brightness,
                      contrast: contrast,
                      prj_name: prj_name,
                      prj_comments: prj_comments,
                      price: totalpric,
                      three_five_frm_cost:three_five_frm_cost,
                      effects_price:effects_cost,
                      digitalcorrections: digitalcorrections,
                      major_retouching:major_retouching,
                      minor_change_array:minor_change_array,
                      originalimg: original_img,
                      editedimg: edited_img
                    },
                    beforeSend : function() {
                       jQuery('#loader').modal('show');

                         // $(".post_submitting").show().html("<center><img src='images/loading.gif'/></center>");
                    },
                    success : function(response) {
                     //   alert(response);
                     //   $("#return_update_msg").html(response.product_id);
                     //   $(".post_submitting").fadeOut(1000);

                      console.log(response);
                     console.log(response.product_id);
                     jQuery.ajax({
                        type: 'GET',
                        url:  "<?php echo Mage::getBaseUrl();?>checkout/cart/add",
                        dataType: 'json',
                        data : {
                          product: response.product_id,
                          qty: 1
                        },
                        success: function(result) {
                          console.log(result);
                          if(result.r=='success'){
                            console.log(result);
                            alert(result.message);
                            jQuery('#loader').modal('hide');
                          }else{
                            alert(result.message);
                            jQuery('#loader').modal('hide');
                          }
                           
                        }
                    });

                     
                    }
                });



    }



   }else{
    alert('please select at least one image');
   }
      

});


//add to cart ends



// ajax login 




jQuery('#loginbtnclick').click(function(){

    var email=jQuery('#loginEmail').val();
    var password=jQuery('#loginPass').val();


if(email.length > 0 && email != ''){
  if(password.length > 0 && password != ''){

    jQuery.ajax({
                  type : "POST",
                  url : "<?php echo Mage::getBaseUrl();?>mymodule/index/loginuser",
                    dataType: 'json',
                    data : {
                      email: email,
                      password: password
                    },
                    beforeSend : function() {
                        jQuery('#loader').modal('show');
                    },
                    success : function(response) {
                      jQuery('#loader').modal('hide');
                      if(response.status==false){
                        alert(response.msg);
                       jQuery('#loginerrmsg').html(response.msg); 
                      }else{



var id1=jQuery('.activeimage').children('img').attr("alt");
 
   if(id1.length > 0 && id1 != ''){
   
    //start
    //default fields
    var category_id= jQuery("input[name='category_id']").val();
    var order_type= jQuery("input[name='order_type']").val();
    var type= jQuery("input[name='type"+id1+"']").val();


    //image data 
    var original_img=jQuery('.activeimage').children('img').attr("src");
    var edited_img=jQuery("#edited_form_img").attr('src');


    //4rth tab
    var canvas_type= jQuery("#subtype option:selected").val();
    var media= jQuery("#media option:selected").val();
    var frame= jQuery("#frame option:selected").val();

    var sizeselected=jQuery("#size option:selected").val();
        sizeselected = sizeselected.split('#');

        var order_size= sizeselected[0].trim();
        

    var view= jQuery("#view option:selected").val();
    var effects= jQuery("#effect option:selected").val();
    var rotation= jQuery("#anglehidden").val();
    var brightness= jQuery("#brightnesshidden").val();
    var contrast=jQuery("#contrasthidden").val();

    var prj_name= jQuery("input[name='prj_name']").val();
    var prj_comments= jQuery("textarea[name='prj_comments']").val();


    var digitalcorrections= jQuery("#digitalcorrections").val();
    var major_retouching=' ';
    var minor_change_array=' ';

    if(digitalcorrections=='Minor Retouching'){
      var matches = [];
      jQuery(".radio5:checked").each(function() {
          matches.push(this.value);
          alert(this.value);
      });
      minor_change_array=matches;
    }
    if(digitalcorrections=='Major Retouching'){
      major_retouching=jQuery('#majorretouch').val();
    }
    
  
    // price things
    var three_five_frm_cost=0;
    var price1= jQuery("#price").val();
    var price=0;
var totalpric=jQuery("#price").val();
    if(frame=='GALLERY WRAP CANVAS FRAME'){
        if(canvas_type=='three frame canvas'){
          three_five_frm_cost=price1*0.1;
          price=price1-three_five_frm_cost;
        }
        if(canvas_type=='five frame canvas'){
          three_five_frm_cost=price1*0.3;
          price=price1-three_five_frm_cost;
        }
    }

    var effects_cost=0;
var size_price= price;





            jQuery.ajax({
                  type : "POST",
                  url : "<?php echo Mage::getBaseUrl();?>mymodule/index/imageupload",
                    dataType: 'json',
                    data : {
                      category_id: category_id,
                      order_type: order_type,
                      canvas_type: canvas_type,
                      media: media,
                      type: type,
                      type: type,
                      size: order_size,
                      size_price: size_price,
                      frame: frame,
                      view: view,
                      effects: effects,
                      rotation: rotation,
                      brightness: brightness,
                      contrast: contrast,
                      prj_name: prj_name,
                      prj_comments: prj_comments,
                      price: totalpric,
                      three_five_frm_cost:three_five_frm_cost,
                      effects_price:effects_cost,
                      digitalcorrections: digitalcorrections,
                      major_retouching:major_retouching,
                      minor_change_array:minor_change_array,
                      originalimg: original_img,
                      editedimg: edited_img
                    },
                    beforeSend : function() {
                         // $(".post_submitting").show().html("<center><img src='images/loading.gif'/></center>");
                    },
                    success : function(response) {
                     //   alert(response);
                     //   $("#return_update_msg").html(response.product_id);
                     //   $(".post_submitting").fadeOut(1000);

                      console.log(response);
                     console.log(response.product_id);
                     jQuery.ajax({
                        type: 'GET',
                        url:  "<?php echo Mage::getBaseUrl();?>checkout/cart/add",
                        dataType: 'json',
                        data : {
                          product: response.product_id,
                          qty: 1
                        },
                        success: function(result) {
                          console.log(result);
                          if(result.r=='success'){
                            console.log(result);
                            alert(result.message);







                          }else{
                            alert(result.message);
                          }
                           jQuery('#loader').modal('hide');
                        }
                    });

                     
                    }
                });




   }else{
    alert('please select at least one image');
   }
      


                        jQuery('#loginmdl').modal('hide');
                        
                      }

                    }
                });




  }
}


            

});

//ajax login ends

  });

