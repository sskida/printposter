importFile = false;

hideDialog = function (dialog) {
	jQuery(dialog).hide();
	if (jQuery('.dialog:visible').length == 0) jQuery('#overlay').hide();
	editText = false;
}

showDialog = function (dialog) {
	jQuery('#overlay').show();
	jQuery(dialog).show();
}

jQuery(window).resize(function () {
	jQuery('.dialog').each(function () {
		jQuery(this).css({ left: window.innerWidth / 2 - jQuery(this).outerWidth() / 2 + 'px', top: window.innerHeight / 2 - jQuery(this).outerHeight() / 2 + 'px' });
	});
	
	jQuery('canvas').attr('height', jQuery(window).height() - 37).attr('width', jQuery(window).width() - 232);
	//jQuery('ul#mainmenu').width(jQuery(window).width() - 4);
	jQuery('ul#layers').css({ height: jQuery(window).height() - 37 });
	
	app.refreshLayers();
	
	if (jQuery('#cropoverlay').css('display') == 'block') {
		jQuery('#cropoverlay').css({ 
			left: Math.ceil(app.canvas.width / 2 - app.getActiveLayer().x - app.getActiveLayer().regX - 1) + 'px', 
			top: Math.ceil(app.canvas.height / 2 + app.getActiveLayer().y - app.getActiveLayer().regY + 38) + 'px'
		});
	}
});
	
jQuery(document).ready(function () {
	jQuery("ul#mainmenu li button").click(function () {
		jQuery(this).focus();
		jQuery(this).parent().find("ul.submenu:visible").slideUp('fast').show();
		jQuery(this).parent().find("ul.submenu:hidden").slideDown('fast').show();
	});
	
	jQuery("ul#mainmenu li button").blur(function () {
		jQuery(this).parent().find("ul.submenu:visible").delay(100).slideUp('fast').show();
	});

	jQuery('#button-openfile').hover(
		function () { jQuery(this).addClass('hover'); },
		function () { jQuery(this).removeClass('hover'); }
	);

	jQuery('#button-importfile').hover(
		function () { jQuery(this).addClass('hover'); },
		function () { jQuery(this).removeClass('hover'); }
	);

	jQuery('#button-openurl').click(function () {
		importFile = false;
		showDialog('#dialog-openurl');
		jQuery('#dialog-openurl input').val('').attr('disabled', false).focus();
	});

	jQuery('#button-importurl').click(function () {
		importFile = true;
		showDialog('#dialog-openurl');
		jQuery('#dialog-openurl input').val('').attr('disabled', false).focus();
	});
	
	jQuery('#button-undo').click(function () { app.undo(); });
	jQuery('#button-redo').click(function () { app.redo(); });
	
	jQuery('#button-layerscale').click(function () {
		affectImage = false;
		showDialog('#dialog-scale');
		jQuery('#dialog-scale input.input-scaleX').val('100');
		jQuery('#dialog-scale input.input-scaleY').val('100');
	});
	
	jQuery('#button-layerskew').click(function () {
		affectImage = false;
		showDialog('#dialog-skew');
		jQuery('#dialog-skew input.input-scaleX').val('100');
		jQuery('#dialog-skew input.input-scaleY').val('100');
	});
	
	jQuery('#button-layerrotate').click(function () {
		affectImage = false;
		showDialog('#dialog-rotate');
		jQuery('#dialog-rotate input').val('0');
	});
	
	jQuery('#button-layercrop').click(function () {
		affectImage = false;
		app.sortLayers();
		app.refreshLayers();
		var layer = app.getActiveLayer();
		console.log(layer);
		jQuery('#overlay').show();
		jQuery('#cropoverlay').css({
			left: Math.ceil(app.canvas.width / 2 + layer.x - layer.regX - 1) + 'px',
			top: Math.ceil(app.canvas.height / 2 + layer.y - layer.regY + 38) + 'px', 
			width: (layer.text != null ? layer.getMeasuredWidth() * layer.scaleX: layer.image.width * layer.scaleX) + 2 + 'px', 
			height: (layer.text != null ? layer.getMeasuredLineHeight() * layer.scaleY: layer.image.height * layer.scaleY) + 2 + 'px'
		}).show();
	});
	
	jQuery('#button-layerflipv').click(app.callbacks.layerFlipV);
	jQuery('#button-layerfliph').click(app.callbacks.layerFlipH);
	
	jQuery('#button-imagescale').click(function () {
		affectImage = true;
		showDialog('#dialog-scale');
		jQuery('#dialog-scale input.input-scaleX').val('100');
		jQuery('#dialog-scale input.input-scaleY').val('100');
	});
	
	jQuery('#button-imageskew').click(function () {
		affectImage = true;
		showDialog('#dialog-skew');
		jQuery('#dialog-skew input.input-scaleX').val('100');
		jQuery('#dialog-skew input.input-scaleY').val('100');
	});
	
	jQuery('#button-imagerotate').click(function () {
		affectImage = true;
		showDialog('#dialog-rotate');
		jQuery('#dialog-rotate input').val('0');
	});
	
	jQuery('#button-imageskew').click(function () {
		affectImage = true;
		showDialog('#dialog-skew');
		jQuery('#dialog-skew input.input-skewX').val('0');
		jQuery('#dialog-skew input.input-skewY').val('0');
	});
	
	jQuery('#button-filterbrightness').click(function () {
		showDialog('#dialog-filterbrightness');
		jQuery('#dialog-filterbrightness input').val('100');
	});
	
	jQuery('#button-filtercolorify').click(function () {
		showDialog('#dialog-filtercolorify');
		jQuery('#dialog-filtercolorify input').val('0');
	});
	
	jQuery('#button-filterblur').click(function () {
		showDialog('#dialog-filterblur');
		jQuery('#dialog-filterblur input').val('1');
	});
	
	jQuery('#button-filtergaussianblur').click(function () {
		showDialog('#dialog-filtergaussianblur');
		jQuery('#dialog-filtergaussianblur input.7').attr('checked', true);
	});
	
	jQuery('#button-executescript').click(function () {
		showDialog('#dialog-executescript');
		jQuery('#dialog-executescript textarea').val('');
	});
	
	jQuery('#button-select').click(function () {
		app.tool = TOOL_SELECT;
		jQuery('#mainmenu button').removeClass('active');
		jQuery(this).addClass('active');
	});
	
	jQuery('#button-move').click(function () {
		app.tool = TOOL_MOVE;
		jQuery('#mainmenu button').removeClass('active');
		jQuery(this).addClass('active');
	});
	
	jQuery('#button-text').click(function () {
		app.tool = TOOL_TEXT;
		jQuery('#mainmenu button').removeClass('active');
		jQuery(this).addClass('active');
	});
	
	jQuery('#button-imageflipv').click(app.callbacks.imageFlipV);
	jQuery('#button-imagefliph').click(app.callbacks.imageFlipH);
	
	jQuery('#dialog-openurl input').keydown(app.callbacks.openURL);
	jQuery('#dialog-openurl button.button-ok').click(app.callbacks.openURL);
	jQuery('#dialog-scale input').keydown(app.callbacks.numberOnly).keydown(app.callbacks.layerScale);
	jQuery('#dialog-scale button.button-ok').click(app.callbacks.layerScale);
	jQuery('#button-openfile input').change(app.callbacks.openFile);
	jQuery('#button-importfile input').change(app.callbacks.importFile);
	jQuery('#dialog-tooltext button.button-ok').click(app.callbacks.toolText);
	jQuery('#dialog-tooltext input').keydown(app.callbacks.toolText);
	jQuery('#dialog-layerrename button.button-ok').click(app.callbacks.layerRename);
	jQuery('#dialog-layerrename input').keydown(app.callbacks.layerRename);
	jQuery('#dialog-rotate button.button-ok').click(function(){
		var deg=jQuery('#dialog-rotate input').val();
		app.addUndo();
		if (affectImage) return imageRotate(deg);
		app.getActiveLayer().rotation += deg;
		hideDialog('#dialog-rotate');
		//jQuery('#img_preview').attr("src", app.getActiveLayer()['image'].currentSrc);
		//jQuery('#button-save').click();
		rotate(deg);	
	});
	jQuery('#dialog-rotate input').keydown(app.callbacks.numberOnly).keydown(app.callbacks.layerRotate);
	jQuery('#dialog-skew button.button-ok').click(app.callbacks.layerSkew);
	jQuery('#dialog-skew input').keydown(app.callbacks.numberOnly).keydown(app.callbacks.layerSkew);
	jQuery('#cropoverlay button.button-ok').click(app.callbacks.layerCrop);
	jQuery('#button-filterdesaturation').click(app.callbacks.filterDesaturation);
	jQuery('#button-filteredgedetection').click(app.callbacks.filterEdgeDetection);
	jQuery('#button-filteredgeenhance').click(app.callbacks.filterEdgeEnhance);
	jQuery('#button-filteremboss').click(app.callbacks.filterEmboss);
	jQuery('#button-filtersharpen').click(app.callbacks.filterSharpen);
	jQuery('#dialog-filterbrightness button.button-ok').click(app.callbacks.filterBrightness);
	jQuery('#dialog-filterbrightness input').keydown(app.callbacks.numberOnly).keydown(app.callbacks.filterBrightness);
	jQuery('#dialog-filtergaussianblur button.button-ok').click(app.callbacks.filterGaussianBlur);
	jQuery('#dialog-filtergaussianblur input').keydown(app.callbacks.numberOnly).keydown(app.callbacks.filterGaussianBlur);
	jQuery('#dialog-filterblur button.button-ok').click(app.callbacks.filterBlur);
	jQuery('#dialog-filterblur input').keydown(app.callbacks.numberOnly).keydown(app.callbacks.filterBlur);
	jQuery('#dialog-filtercolorify button.button-ok').click(app.callbacks.filterColorify);
	jQuery('#dialog-filtercolorify input').keydown(app.callbacks.numberOnly).keydown(app.callbacks.filterColorify);
	jQuery('#dialog-executescript button.button-ok').click(app.callbacks.scriptExecute);
	jQuery('#button-save').click(app.callbacks.saveFile);
	jQuery('#button-print').click(app.callbacks.printFile);






	
	jQuery('#dialog-tooltext input.input-color').keyup(function (e) {
		jQuery(this).css({ backgroundColor: jQuery(this).val() });
	});
	
	jQuery('ul#layers li').live('click', function () {
		app.activateLayer(jQuery(this).attr('id').replace('layer-', '') * 1);
	});
	
	jQuery('ul#layers li button.button-delete').live('click', function () {
		app.layers.splice(jQuery(this).parent().parent().attr('id').replace('layer-', '') * 1, 1);
		this.undoBuffer = [];
		this.redoBuffer = [];
		app.refreshLayers();
	});
	
	jQuery('ul#layers li button.button-hide').live('click', function () {
		if (jQuery(this).text() == 'Hide') {
			app.layers[jQuery(this).parent().parent().attr('id').replace('layer-', '') * 1].visible = false;
		} else {
			app.layers[jQuery(this).parent().parent().attr('id').replace('layer-', '') * 1].visible = true;
		}
		app.refreshLayers();
	});
	
	jQuery('ul#layers li button.button-rename').live('click', function () {
		jQuery('#dialog-layerrename').show();
		jQuery('#overlay').show();
		jQuery('#dialog-layerrename input').val('');
		app.renameLayer = jQuery(this).parent().parent().attr('id').replace('layer-', '') * 1;
	});




jQuery(".radio").live('click', function () {
	if(app.getActiveLayer()){

  var contentId = jQuery(this).siblings("input[name='size']").val();
  var value=contentId.split("*");
	var height=value[0].trim();
    var width=value[1].trim();
    var newht= (parseInt(height)*96)/11.86;
    var newwdth= (parseInt(width)*96)/11.86;
   jQuery('#preview_area').height(newht);
   jQuery('#preview_area').width(newwdth);
   jQuery('#preview_area').css('margin-top',(350-newht)/2);
var image = new Image();
image.src = jQuery('#img_preview').attr("src");
	var imageht=image.naturalHeight;
	var imagewdt=image.naturalWidth;
	if(parseInt(newwdth/newht)>parseInt(imagewdt/imageht)){
		jQuery('#img_preview').attr("style","height:100%;");
		var left=(newwdth-jQuery('#img_preview').width())/2;
		jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");
	}else{
		jQuery('#img_preview').attr("style","width:100%;");
		var margin=(newht-jQuery('#img_preview').height())/2;
		if(margin>0){
			jQuery('#img_preview').attr("style","width:100%;margin-top:"+margin+"px;");
			jQuery('#frame_png').attr("style","left:0px; top:"+margin+"px; width:100%; height:"+jQuery('#img_preview').height()+"px;");
		}else{
			jQuery('#img_preview').attr("style","height:100%;");
			var left=(newwdth-jQuery('#img_preview').width())/2;
			jQuery('#frame_png').attr("style","height:100%; top:0px; left:"+left+"px; width:"+jQuery('#img_preview').width()+"px;");
		}
		
	}

imageht=0;
imagewdt=0;
newwdth=0;
newht=0;
height=0;
width=0;



		/*affectImage = false;
		app.sortLayers();
		app.refreshLayers();
		var layer = app.getActiveLayer();
		
	  if(layer!=""){
	  var contentId = jQuery(this).siblings("input[name='size']").val();
      var value=contentId.split("*");
      console.log(value);
      console.log(layer);

     jQuery.ajax({          
            type: 'POST',
            url: 'http://localhost/printposters/index.php/mymodule/index/resizeimage',
            data: {
                height: value[0].trim(),
                width: value[1].trim(),
                type: value[2].trim(),
                file: layer['image'].currentSrc
            },
            datatype:'json',
            success: function(response) {
                
            	var img = new Image();
		
				img.onload = function () {
					app.layers = [];
					app.layers[0] = new Bitmap(img);
					app.layers[0].x = 0;
					app.layers[0].y = 0;
					app.activateLayer(0);
				}
				img.src = response;
				this.undoBuffer = [];
				this.redoBuffer = [];
				console.log(app.getActiveLayer());

				layerScale(0.1 * 1, 0.1 * 1);




            },error: function(){
               
            }
        });









	  }else
	  {
	  	alert("please select image");
	  }
      */
      		
	}else{
		jQuery(this).removeAttr('checked');
		alert("please select image");
	}
  });









	
	
	jQuery(document).keydown(function (e) {
		if (e.keyCode == 27) {
			hideDialog('.dialog');
		}
	});
	
	jQuery('.dialog button.button-cancel').each(function () {
		jQuery(this).click(function () {
			hideDialog(jQuery(this).parent());
		});
	});
	
	jQuery('canvas').click(app.callbacks.toolText);
	
	jQuery('#cropoverlay').draggable().resizable({
		aspectRatio: 1 / 1,
		handles: 'se',
		//var layer1=app.getActiveLayer();
		resize: function (e, ui) {
			//jQuery('#cropoverlay').css({ left: layer1['_matrix'].tx + 'px', top: layer1['_matrix'].ty + 'px' });
		},
		stop: function (e, ui) {
			//jQuery('#cropoverlay').css({ left: ui.position.left + 'px', top: ui.position.top + 'px' });
		}
	});
	
	jQuery(window).resize();









 var lastdeg=0;
var elie = null;
    jQuery(function () {
        elie1 = jQuery("#preview_area");
       
    });
    function rotate(degree) {
        console.log(degree);
        alert(lastdeg);
       lastdeg=parseInt(lastdeg)+parseInt(degree);
    
        elie1.css({
            WebkitTransform: 'rotate(' + lastdeg + 'deg)'
        });
        elie1.css({
            '-moz-transform': 'rotate(' + lastdeg + 'deg)'
        });
    } 












});