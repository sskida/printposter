openFile = function (url, first) {
	var img = new Image();
		
	img.onload = function () {
		var n = (first ? 0: app.layers.length);
		if (first) app.layers = [];
		app.layers[n] = new Bitmap(img);
		app.layers[n].x = 0;
		app.layers[n].y = 0;
		app.activateLayer(n);
	}
	img.src = url;
	jQuery('#img_preview').attr("src", url);
	this.undoBuffer = [];
	this.redoBuffer = [];
	console.log(app);
	jQuery('canvas').attr('height', img.height).attr('width', img.width);
}

openURL = function (self, url) {
	jQuery(self).attr('disabled', true);
	openFile(url, !importFile);
	hideDialog('#dialog-openurl');
}

saveFile = function () {
	alert();
	var image=app.stage.toDataURL();
	jQuery('#img_preview').attr("src", image);
}

printFile = function () {
	window.print()
}

app.callbacks.openFile = function (e) {
	var file = e.target.files[0],
		self = this;
	
	if (!file.type.match('image.*')) return false;

	var reader = new FileReader();
	reader.onload = function(e) {
		openFile(e.target.result, true);
	};

	reader.readAsDataURL(file);
};

app.callbacks.openURL = function (e) {
	switch (e.type) {
		case "click":
			openURL(jQuery('#dialog-openurl input'), jQuery('#dialog-openurl input').val());
			break;
		case "keydown":
			if (e.keyCode == 13) openURL(this, jQuery(this).val());
			break;
	}
}

app.callbacks.importFile = function (e) {
	for (var i = 0, file; file = e.target.files[i]; i++) {
		if (!file.type.match('image.*')) continue;

		var reader = new FileReader();
		reader.onload = function(e) {
			openFile(e.target.result, false);
		};

		reader.readAsDataURL(file);
	}
};

app.callbacks.saveFile = function () {
	saveFile();
}

app.callbacks.printFile = function () {
	printFile();
}


$(document).ready(function($) {
    $.extend( true, jQuery.fn, {        
        imagePreview: function( options ){          
            var defaults = {};
            if( options ){
                $.extend( true, defaults, options );
            }
            $.each( this, function(){
                var $this = $( this );              
                $this.bind( 'change', function( evt ){

                    var files = evt.target.files; // FileList object
                    // Loop through the FileList and render image files as thumbnails.
                    for (var i = 0, f; f = files[i]; i++) {
                        // Only process image files.
                        if (!f.type.match('image.*')) {
                        continue;
                        }
                        var reader = new FileReader();
                        // Closure to capture the file information.
                        reader.onload = (function(theFile) {
                            return function(e) {
                                // Render thumbnail.
                                    $('#imageURL').attr('src',e.target.result);                         
                            };
                        })(f);
                        // Read in the image file as a data URL.
                        reader.readAsDataURL(f);
                    }

                });
            });
        }   
    });
    $( '#fileinput' ).imagePreview();
});
