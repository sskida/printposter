﻿affectImage = false;

layerScale = function (x, y) {
	app.addUndo();
	if (affectImage) return imageScale(x, y);
	app.getActiveLayer().scaleX *= x / 100;
	app.getActiveLayer().scaleY *= y / 100;
	console.log(app.getActiveLayer());
	hideDialog('#dialog-scale');
}

layerRotate = function (deg) {
	app.addUndo();
	if (affectImage) return imageRotate(deg);
	app.getActiveLayer().rotation += deg;
	alert();
	if((deg/90) % 2 === 0){
		jQuery('canvas').attr('height', app.getActiveLayer().height).attr('width', app.getActiveLayer().width);
	}else{
		jQuery('canvas').attr('height', app.getActiveLayer().width).attr('width', app.getActiveLayer().height);
	}
	


	hideDialog('#dialog-rotate');
}

layerSkew = function (degx, degy) {
	app.addUndo();
	if (affectImage) return imageSkew(degx, degy);
	app.getActiveLayer().skewX += degx;
	app.getActiveLayer().skewY += degy;
	hideDialog('#dialog-skew');
}

layerFlipH = function () {
	app.addUndo();
	app.getActiveLayer().scaleX = -app.getActiveLayer().scaleX;
}

layerFlipV = function () {
	app.addUndo();
	app.getActiveLayer().scaleY = -app.getActiveLayer().scaleY;
}


app.callbacks.numberOnly = function (e) {
	if ((e.shiftKey) || ([8, 13, 37, 38, 39, 40, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 190, 189].indexOf(e.keyCode) < 0)) return false;
}

app.callbacks.layerRename = function (e) {
	console.log(app.getActiveLayer());
	var contentId = jQuery(this).siblings("input[name='size']").val();
    var value=contentId.split("*");
}

app.callbacks.layerScale = function (e) {
	switch (e.type) {
		case "click":
			layerScale(jQuery('#dialog-scale input.input-scaleX').val() * 1, jQuery('#dialog-scale input.input-scaleY').val() * 1);
			break;
		case "keydown":
			if (e.keyCode == 13) layerScale(jQuery('#dialog-scale input.input-scaleX').val() * 1, jQuery('#dialog-scale input.input-scaleY').val() * 1);
			break;
	}
}

app.callbacks.layerRotate = function (e) {
	switch (e.type) {
		case "click":
			layerRotate(jQuery('#dialog-rotate input').val() * 1);
			break;
		case "keydown":
			if (e.keyCode == 13) layerRotate(jQuery(this).val() * 1);
			break;
	}
	
}

app.callbacks.layerSkew = function (e) {
	switch (e.type) {
		case "click":
			layerSkew(jQuery('#dialog-skew input.input-skewX').val() / 100, jQuery('#dialog-skew input.input-skewY').val() / 100);
			break;
		case "keydown":
			if (e.keyCode == 13) layerSkew(jQuery('#dialog-skew input.input-skewX').val() / 100, jQuery('#dialog-skew input.input-skewY').val() / 100);
			break;
	}
}

app.callbacks.layerFlipV = function () { layerFlipV(); }
app.callbacks.layerFlipH = function () { layerFlipH(); }

app.callbacks.layerCrop = function () {
	var layer = app.getActiveLayer();
	console.log(layer);
	console.log(app.canvas.width / 2 - jQuery('#cropoverlay').position().left - layer.regX + layer.x - 1);
	layer.cache(
		Math.floor(app.canvas.width / 2 - jQuery('#cropoverlay').position().left - layer.regX + layer.x - 1),
		//Math.floor(app.canvas.height / 2 - jQuery('#cropoverlay').position().top - layer.regY + layer.y + 38),
	//	Math.floor(jQuery('#cropoverlay').position().left-((layer['_matrix'].tx+layer['_matrix'].tx*0.04)*0.5)+10),//92
		Math.floor(jQuery('#cropoverlay').position().top-((layer['_matrix'].ty+layer['_matrix'].ty*0.04)*0.5)+44),
		
		jQuery('#cropoverlay').width()*2-(jQuery('#cropoverlay').width()/2),
		jQuery('#cropoverlay').height()*2-(jQuery('#cropoverlay').height()/2)
	);
	//jQuery(this).parent().find('.button-cancel').click();
}

