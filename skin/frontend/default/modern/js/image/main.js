const 
	TOOL_MOVE = 0,
	TOOL_SELECT = 1,
	TOOL_TEXT = 2;
	
app = {
	stage: null,
	canvas: null,
	layers: [],
	tool: TOOL_SELECT,
	callbacks: {},
	selection: {
		x: -1, x: -1
	},
	renameLayer: 0,
	undoBuffer: [],
	redoBuffer: [],
	
	addUndo: function () {
		this.undoBuffer.push(this.layers.toString());
		this.redoBuffer = [];
	},
	
	loadLayers: function (from, to) {
		var json, jsonString = from.pop();
		if (jsonString == undefined) return false;
		to.push(this.layers.toString());
		json = JSON.parse(jsonString);
		for (var i = 0, layer, jsonLayer; ((layer = this.layers[i]) && (jsonLayer = json[i])); i++) {
			for (value in jsonLayer) {
				if (value != 'filters')	{
					layer[value] = jsonLayer[value];
				} else {
					var hadFilters = (layer.filters != null && layer.filters.length > 0);
					layer.filters = [];
					for (var j = 0; j < jsonLayer.filters.names.length; j++) {
						if (jsonLayer.filters.names[j] == null) break;
						layer.filters[j] = new window[jsonLayer.filters.names[j]];
						for (value2 in jsonLayer.filters.values[0][j]) {
							layer.filters[j][value2] = jsonLayer.filters.values[0][j][value2];
						}
						hadFilters = true;
					}
					if (hadFilters) {
						if (layer.cacheCanvas) {
							layer.updateCache();
						} else {
							layer.cache(0, 0, layer.width, layer.height);
						}
					}
				}
			}
		}
		this.refreshLayers();
	},
	
	undo: function () {
		this.loadLayers(this.undoBuffer, this.redoBuffer);
	},
	
	redo: function () {
		this.loadLayers(this.redoBuffer, this.undoBuffer);
	},
	
	getActiveLayer: function () {
		var ret;
		this.layers.forEach(function(v) {
			if (v.active) ret = v;
		});
		return ret || this.layers[0];
	},
	
	getActiveLayerN: function () {
		for (var i = 0, layer; layer = this.layers[i]; i++) {
			if (layer.active) return i;
		}
	},
	
	activateLayer: function (layer) {
		this.layers.forEach(function (v) {
			v.active = false;
		});
		if (layer instanceof Bitmap) {
			layer.active = true;
		} else  {
			if (this.layers[layer] == undefined) return;
			this.layers[layer].active = true;
		}
		this.refreshLayers();
	},
	
	refreshLayers: function () {
		if ((this.getActiveLayer() == undefined) && (this.layers.length > 0)) this.layers[0].active = true;
		this.stage = new Stage(this.canvas);
		this.stage.regX = -this.canvas.width / 2;
		this.stage.regY = -this.canvas.height / 2;
		

		app.layers.toString = function () {
			var ret = [];
			for (var i = 0, layer; layer = this[i]; i++) {
				ret.push('{"x":' + layer.x + ',"y":' + layer.y + ',"scaleX":' + layer.scaleX + ',"scaleY":' + layer.scaleY + ',"skewX":' + layer.skewX + ',"skewY":' + layer.skewY + ',"active":' + layer.active + ',"visible":' + layer.visible + ',"filters":{"names":[' + (layer.filters != null ? layer.filters.toString().replace(/(\[|\])/g, '"'): 'null') + '],"values":[' + JSON.stringify(layer.filters) + ']}}');
			}
			return '[' + ret.join(',') + ']';
		}
		
		jQuery('ul#layers').html('');
		for (var i = 0, layer; layer = this.layers[i]; i++) {
			var self = this;
			self.stage.addChild(layer);
			(function(t, n) {
				layer.onClick = function (e) {
					if ((self.tool != TOOL_TEXT) || (!t.text)) return true;
					self.activateLayer(t);
					editText = true;
				}
				
				layer.onPress = function (e1) {
					if (self.tool == TOOL_SELECT) {
						self.activateLayer(t);
					}
					
					var	offset = {
						x: t.x - e1.stageX,
						y: t.y - e1.stageY
					}
					
					if (self.tool == TOOL_MOVE) self.addUndo();
					
					e1.onMouseMove = function (e2) {
						if (self.tool == TOOL_MOVE) {
							t.x = offset.x + e2.stageX;
							t.y = offset.y + e2.stageY;
						}
					}
				};
			})(layer, i);
			layer.width = (layer.text != null ? layer.getMeasuredWidth() * layer.scaleX: layer.image.width * layer.scaleX);
			layer.height = (layer.text != null ? layer.getMeasuredLineHeight() * layer.scaleY: layer.image.height * layer.scaleY);
			layer.regX = layer.width / 2;
			layer.regY = layer.height / 2;
			jQuery('ul#layers').prepend('<li id="layer-' + i + '" class="' + (layer.active ? 'active': '') + '"><img src="' + (layer.text != undefined ? '': layer.image.src) + '"/><h1>' + ((layer.name != null) && (layer.name != '') ? layer.name: 'Unnamed layer') + '</h1><span><button class="button-delete">Delete</button><button class="button-hide">' + (layer.visible ? 'Hide': 'Show') + '</button><button class="button-rename">Rename</button></span></li>');
		}
		this.stage.update();
		jQuery('ul#layers').sortable({
			stop: function () {
				app.sortLayers();
			}
		});
		
		if (this.layers.length > 0) {
			jQuery('#button-layercrop').attr('disabled', false);
			jQuery('#button-layerscale').attr('disabled', false);
			jQuery('#button-layerrotate').attr('disabled', false);
			jQuery('#button-layerskew').attr('disabled', false);
			jQuery('#button-layerflipv').attr('disabled', false);
			jQuery('#button-layerfliph').attr('disabled', false);
			jQuery('#button-imagescale').attr('disabled', false);
			jQuery('#button-imagerotate').attr('disabled', false);
			jQuery('#button-imageskew').attr('disabled', false);
			jQuery('#button-imageflipv').attr('disabled', false);
			jQuery('#button-imagefliph').attr('disabled', false);
			jQuery('#button-filterbrightness').attr('disabled', false);
			jQuery('#button-filtercolorify').attr('disabled', false);
			jQuery('#button-filterdesaturation').attr('disabled', false);
			jQuery('#button-filterblur').attr('disabled', false);
			jQuery('#button-filtergaussianblur').attr('disabled', false);
			jQuery('#button-filteredgedetection').attr('disabled', false);
			jQuery('#button-filteredgeenhance').attr('disabled', false);
			jQuery('#button-filteremboss').attr('disabled', false);
			jQuery('#button-filtersharpen').attr('disabled', false);
		} else {
			jQuery('#button-layercrop').attr('disabled', true);
			jQuery('#button-layerscale').attr('disabled', true);
			jQuery('#button-layerrotate').attr('disabled', true);
			jQuery('#button-layerskew').attr('disabled', true);
			jQuery('#button-layerflipv').attr('disabled', true);
			jQuery('#button-layerfliph').attr('disabled', true);
			jQuery('#button-imagescale').attr('disabled', true);
			jQuery('#button-imagerotate').attr('disabled', true);
			jQuery('#button-imageskew').attr('disabled', true);
			jQuery('#button-imageflipv').attr('disabled', true);
			jQuery('#button-imagefliph').attr('disabled', true);
			jQuery('#button-filterbrightness').attr('disabled', true);
			jQuery('#button-filtercolorify').attr('disabled', true);
			jQuery('#button-filterdesaturation').attr('disabled', true);
			jQuery('#button-filterblur').attr('disabled', true);
			jQuery('#button-filtergaussianblur').attr('disabled', true);
			jQuery('#button-filteredgedetection').attr('disabled', true);
			jQuery('#button-filteredgeenhance').attr('disabled', true);
			jQuery('#button-filteremboss').attr('disabled', true);
			jQuery('#button-filtersharpen').attr('disabled', true);
		}
	},
	
	sortLayers: function () {
		var tempLayers = [],
			layersList = jQuery('ul#layers li');
			
		for (var i = 0, layer; layer = jQuery(layersList[i]); i++) {
			if (layer.attr('id') == undefined) break;
			tempLayers[i] = this.layers[layer.attr('id').replace('layer-', '') * 1];
		}
		
		tempLayers.reverse();
		this.layers = tempLayers;
		this.refreshLayers();
	}
}

tick = function () {
	app.stage.update();
}

jQuery(document).ready(function () {
	app.canvas = jQuery('canvas')[0];
	
	document.onselectstart = function () { return false; };
	
	Ticker.setFPS(30);
	Ticker.addListener(window);





	console.log(app.getActiveLayer());
});