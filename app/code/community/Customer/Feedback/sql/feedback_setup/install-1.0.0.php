<?php
$installer=$this;
$installer->startSetup();

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('feedback')};
CREATE TABLE {$this->getTable('feedback')} (
  `feedback_id` int(11) unsigned NOT NULL auto_increment COMMENT 'Q&A ID',
  `customer_name` varchar(255) NOT NULL COMMENT 'Customer Name',
  `customer_city` varchar(255) NOT NULL  COMMENT 'Customer City',
  `comment` text NOT NULL  COMMENT 'Comment',
  `status` smallint(6) NOT NULL default '0' COMMENT 'Status',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");
$installer->endSetup();
?>