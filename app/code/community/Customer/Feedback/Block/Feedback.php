<?php
class Customer_Feedback_Block_Feedback extends Mage_Core_Block_Template{
	protected $_Collection=null;
	public function getCollection(){
		if(is_null($this->_Collection)){
			$this->_Collection=Mage::getModel('feedback/feedback')->getCollection();
		}
		return $this->_Collection;
	}
}