<?php

// front action, for using popup-template. for layout updates look into design/frontend/...

class LayoutBuddy_Layoutable_Adminhtml_Sales_Order_PopupController extends Mage_Core_Controller_Front_Action
{
    
    public function embedAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

}
