<?php

class LayoutBuddy_Layoutable_Adminhtml_Sales_Order_EditController extends Mage_Adminhtml_Controller_Action
{
    
    public function getpriceAction()
    {
        $response = '';
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $result = Mage::getModel('layoutable/api')->jsonRequest('details', array('id' => $id), true);
                $response = number_format($result['price'], 2);    
            } catch (Exception $e) {
                // nothing...   
            }
        }
        $this->getResponse()->setBody($response);
    }

}
