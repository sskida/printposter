<?php

require_once 'Mage/Checkout/controllers/CartController.php';

class LayoutBuddy_Layoutable_Checkout_CartController extends Mage_Checkout_CartController
{
    
    public function updateItemOptionsAction()
    {
        if (Mage::getStoreConfig('layoutable/display/replace_cart_item')) {
            parent::updateItemOptionsAction();
        } else {
            parent::addAction();
        }
    }
    
}