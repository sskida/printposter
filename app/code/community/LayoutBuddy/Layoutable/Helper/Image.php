<?php

class LayoutBuddy_Layoutable_Helper_Image extends Mage_Core_Helper_Abstract
{

    protected $_id = null;

    public function init($item, $attributeName)
    {
        if ($this->_id = Mage::helper('layoutable')->getLayoutId($item)) {
            return $this;
        }
        // refer to original helper
        return Mage::helper('catalog/image')->init($item->getProduct(), $attributeName);
    }
    
    public function getId()
    {
        return $this->_id;
    }
    
    public function setId($id)
    {
        $this->_id = $id;
    }
    
    public function resize($width, $height = null)
    {
        return $this;
    }
    
    public function keepAspectRatio($flag)
    {
        return $this;
    }
    
    public function keepFrame($flag, $position = array('center', 'middle'))
    {
        return $this;
    }
    
    public function keepTransparency($flag, $alphaOpacity = null)
    {
        return $this;
    }
    
    public function constrainOnly($flag)
    {
        return $this;
    }
    
    public function backgroundColor($colorRGB)
    {
        return $this;
    }
    
    public function rotate($angle)
    {
        return $this;
    }
    
    public function watermark($fileName, $position, $size=null)
    {
        return $this;
    }
    
    public function setWatermarkSize($size)
    {
        return $this;
    }
    
    public function setWatermarkImageOpacity($size)
    {
        return $this;
    }
    
    public function __toString()
    {
        return LayoutBuddy_Layoutable_Model_Config::getThumbnailImage($this->getId());
    }

}