<?php

class LayoutBuddy_Layoutable_Helper_Data extends Mage_Core_Helper_Abstract
{
    
    public function getWidgetsDataAttributes($item)
    {
        $params = array(       
            'id' => $this->getLayoutId($item),
            'order_code' => $this->getOrderCode($item),
            'item_code' => $this->getItemCode($item),
            'item_number' => $this->getItemNumber($item),
            'item_qty' => $this->getItemQty($item),
            'show_options' => $this->getShowOptionsForWidget($item->getOrder()->getStore()),
        );
        
        $params = Mage::getModel('layoutable/api')->buildSignedParamArray('widget', $params, $this->getPublicKey($item->getOrder()->getStore()), $this->getPrivateKey($item->getOrder()->getStore()));
        
        $dataParams = array_map(
            create_function('$key, $value', 'return "data-" . $key . "=\"" . $value. "\"";'),
            array_keys($params),
            array_values($params)
        );
        
        return implode($dataParams, ' ');
    }
    
    public function isLayoutBuddyItem($item)
    {
        return $this->getLayoutId($item) != null;
    }
    
    public function getLayoutId($item)
    {
        $id = null;
        
        if ($item instanceof Mage_Sales_Model_Quote_Item) {
            // Item in cart
            $buyRequestOption = $item->getOptionByCode('info_buyRequest');
            $buyRequest = unserialize($buyRequestOption->getValue());
            if ((isset($buyRequest['layoutbuddy_id'])) && ($buyRequest['layoutbuddy_id'])) {
                $id = $buyRequest['layoutbuddy_id'];
            }
            
        } else if ($item instanceof Mage_Sales_Model_Order_Item) {
            // Ordered item
            $buyRequest = $item->getProductOptionByCode('info_buyRequest');
            if ((isset($buyRequest['layoutbuddy_id'])) && ($buyRequest['layoutbuddy_id'])) {
                $id = $buyRequest['layoutbuddy_id'];
            }
        }
        
        return $id;
    }
    
    public function getOrderCode($orderItem)
    {
        return $orderItem->getOrder()->getIncrementId();
    }
    
    public function getItemCode($orderItem)
    {
        return $orderItem->getId();
    }
    
    public function getItemNumber($orderItem)
    {
        $order = $orderItem->getOrder();
        $items = $order->getItemsCollection();
        $counter = 1;
        foreach ($items as $item) {
            if ($item->getId() == $orderItem->getId()) {
                break;
            }
            $counter++;
        }
        return $counter;
    }
    
    public function getItemQty($orderItem)
    {
        return (int)$orderItem->getQtyOrdered();
    }
    
    public function getPrivateKey($store = null)
    {
        if ($store) {
            return $store->getConfig('layoutable/account/private_key');
        } else {
            return Mage::getStoreConfig('layoutable/account/private_key');
        }
    }
    
    public function getPublicKey($store = null)
    {
        if ($store) {
            return $store->getConfig('layoutable/account/public_key');
        } else {
            return Mage::getStoreConfig('layoutable/account/public_key');
        }
    }
    
    public function getShowOptionsForWidget($store = null)
    {
        if ($store) {
            $showOptionsInStore = $store->getConfig('layoutable/display/show_options');
        } else {
            $showOptionsInStore = Mage::getStoreConfig('layoutable/display/show_options');
        }
        $showOptionsForWidget = (int)($showOptionsInStore == false);
        return $showOptionsForWidget;
    }
    
    public function getJsUrl()
    {
        return LayoutBuddy_Layoutable_Model_Config::getJsUrl();
    }
    
    public function getEmbedScript()
    {
        return '<script type="text/javascript" src="' . LayoutBuddy_Layoutable_Model_Config::getJsEmbedUrl() . '"></script>';
    }

}