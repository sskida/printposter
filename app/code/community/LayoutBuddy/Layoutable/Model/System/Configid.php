<?php

class LayoutBuddy_Layoutable_Model_System_Configid extends Varien_Object
{
    
    static public function getOptionArray($website = null)
    {
        try {
            $configs = Mage::getModel('layoutable/api')->setWebsite($website)->jsonRequest('listConfigs', array(), true, null, $website);
            $result = array();
            foreach ($configs as $config) {
                $result[$config['id']] = $config['id'] . ': ' . $config['name'] . ' (' . $config['base'] . ')';
            }
            return $result;
        } catch (Exception $e) {
            return array();    
        }
    }

    static public function getAllOptions()
    {
        $res = array();
        $res[] = array('value' => '', 'label' => Mage::helper('catalog')->__('-- Please Select --'));

        $publicKeys = array();
        foreach (Mage::app()->getWebsites() as $website) {
            $publicKeys[] = $website->getConfig('layoutable/account/public_key');
        }
        if (count(array_unique($publicKeys)) === 1) {
            // only one account, get result for default keys
            foreach (self::getOptionArray() as $index => $value) {
                $res[] = array(
                   'value' => $index,
                   'label' => $value
                );
            }
        } else {
            // multiple accounts, get results for all keys
            foreach (Mage::app()->getWebsites() as $website) {
                $configIds = array();
                foreach (self::getOptionArray($website) as $index => $value) {
                    $configIds[] = array(
                       'value' => $index,
                       'label' => $value
                    );
                }
                $res[] = array(
                   'value' => $configIds,
                   'label' => $website->getName()
                );
            }
        }
        return $res;
    }

}
