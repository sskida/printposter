<?php

class LayoutBuddy_Layoutable_Model_System_Environment
{
    
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 0, 'label' => 'Production'),
            array('value' => 1, 'label' => 'Sandbox'),
        );
    }

}
