<?php

class LayoutBuddy_Layoutable_Model_Observer extends Mage_Bundle_Model_Observer
{
    
    protected function _setItemOptionsForLayoutable($item)
    {
        
        // Check if LayoutBuddy-ID in request
        $buyRequestOption = $item->getOptionByCode('info_buyRequest');
        $buyRequest = unserialize($buyRequestOption->getValue());
        if ((!isset($buyRequest['layoutbuddy_id'])) || (!$buyRequest['layoutbuddy_id'])) {
            Mage::getSingleton('checkout/session')->setRedirectUrl($item->getProduct()->getProductUrl());
            Mage::throwException(Mage::helper('layoutable')->__('Please specify the product\'s option(s).'));
            //Mage::throwException(Mage::helper('layoutable')->__('The product has to be designed first!'));
        } else {
            
            $id = $buyRequest['layoutbuddy_id'];
            
            $result = Mage::getModel('layoutable/api')->jsonRequest('details', array('id' => $id), true);
            $customPrice = $result['price'];
            
            // Set price
            $item->setCustomPrice($customPrice);
            $item->setOriginalCustomPrice($customPrice);

            // Set quantity
            $item->setAdditionalData(json_encode(array('layoutbuddy_qty' => $result['quantity'])));
            
            $showOptions = Mage::getStoreConfig('layoutable/display/show_options');
            $showOptionSize = Mage::getStoreConfig('layoutable/display/show_option_size');
            
            $options = array();
            // Set sizes
            if ($showOptionSize) {
                $options[] = array('label' => 'Option', 'value' => $result['width'] . 'x' . $result['height'] . $result['unit']);
            }
            // Set options when set to shown
            if ($showOptions) {
                foreach ($result['options'] as $option) {
                    $options[] = array('label' => 'Option', 'value' => $option['qty'] . 'x ' . $option['label'] . ' (' . Mage::helper('core')->formatPrice($option['price'], false) . ')');
                }
            }
            
            $item->addOption(new Varien_Object(
                array(
                    'product' => $item->getProduct(),
                    'code' => 'additional_options',
                    'value' => serialize($options)
                )
            ));
        }
    }
    
    
    /*
    *   Frontend events
    */

    public function checkoutCartUpdateItemComplete($observer)
    {
        $item = $observer->getEvent()->getItem();
        if ($item && $item->getProduct()->getLayoutbuddyConfigId()) {
            $this->_setItemOptionsForLayoutable($item);
            // is saved already, so save updates
            $item->save();
        }
        return $this;
    }
    
    
    public function checkoutCartProductAddAfter($observer)
    {
        $item = $observer->getEvent()->getQuoteItem();
        if ($item && $item->getProduct()->getLayoutbuddyConfigId()) {
            $this->_setItemOptionsForLayoutable($item);
        }
        return $this;
    }
    
    
    public function salesConvertQuoteItemToOrderItem($observer)
    {
        $quoteItem = $observer->getItem();
        if ($additionalOptions = $quoteItem->getOptionByCode('additional_options')) {
            $orderItem = $observer->getOrderItem();
            $options = $orderItem->getProductOptions();
            $options['additional_options'] = unserialize($additionalOptions->getValue());
            $orderItem->setProductOptions($options);
        }
        return $this;
    }


    /*
    *   Admin events
    */

    public function salesConvertOrderItemToQuoteItem($observer)
    {
        $orderItem = $observer->getOrderItem();
        if (Mage::helper('layoutable')->isLayoutBuddyItem($orderItem)) {
            // apply custom price
            if ($orderItem->getOriginalPrice() != $orderItem->getPrice()) {
                $quoteItem = $observer->getQuoteItem();
                if (Mage::getSingleton('tax/config')->priceIncludesTax($quoteItem->getQuote()->getStoreId())) {
                    $price = $orderItem->getPriceInclTax();
                } else {
                    $price = $orderItem->getPrice();
                }
                $quoteItem->setCustomPrice($price);
                $quoteItem->setOriginalCustomPrice($price);
            }
        }
        return $this;
    }

    public function salesQuoteProductAddAfter($observer)
    {
        $items = $observer->getItems();
        foreach ($items as $item) {
            $buyRequestOption = $item->getOptionByCode('info_buyRequest');
            $buyRequest = unserialize($buyRequestOption->getValue());
            if (isset($buyRequest['configured']) && isset($buyRequest['layoutbuddy_id'])) {
                if (!Mage::helper('layoutable')->getLayoutId($item)) {
                    try {
                        $this->_setItemOptionsForLayoutable($item);
                    } catch (Exception $e) {
                        // nothing
                    }
                }
            }
        }
        return $this;
    }

    public function salesQuoteItemSetProduct($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        if ($data = $quoteItem->getAdditionalData()) {
            $data = json_decode($quoteItem->getAdditionalData(), true);
            if (isset($data['layoutbuddy_qty']) && ($quantity = $data['layoutbuddy_qty'])) {
                $quoteItem->setWeight($observer->getProduct()->getWeight() * $quantity);
            }
        }
    }
    
}
