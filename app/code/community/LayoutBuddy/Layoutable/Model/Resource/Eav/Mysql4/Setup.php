<?php

class LayoutBuddy_Layoutable_Model_Resource_Eav_Mysql4_Setup extends Mage_Eav_Model_Entity_Setup
{

    const GROUP_NAME = 'LayoutBuddy';

    public function getDefaultEntities()
    {
        return array(
            'catalog_product' => array(
                'entity_model'      => 'catalog/product',
                'attribute_model'   => 'catalog/resource_eav_attribute',
                'table'             => 'catalog/product',
                'additional_attribute_table' => 'catalog/eav_attribute',
                'entity_attribute_collection' => 'catalog/product_attribute_collection',
                'attributes'        => array(
                    'layoutbuddy_config_id' => array(
                        'group'             => 'Design',
                        'label'             => 'LayoutBuddy Config ID',
                        'type'              => 'int',
                        'input'             => 'text',
                        'default'           => '0',
                        'class'             => '',
                        'backend'           => '',
                        'frontend'          => '',
                        'source'            => '',
                        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'visible'           => true,
                        'required'          => false,
                        'user_defined'      => false,
                        'searchable'        => false,
                        'filterable'        => false,
                        'comparable'        => false,
                        'visible_on_front'  => false,
                        'visible_in_advanced_search' => false,
                        'unique'            => false
                    ),
                    'layoutbuddy_layout_id' => array(
                        'group'             => 'Design',
                        'label'             => 'LayoutBuddy Layout ID (optional)',
                        'type'              => 'varchar',
                        'input'             => 'text',
                        'default'           => '',
                        'class'             => '',
                        'backend'           => '',
                        'frontend'          => '',
                        'source'            => '',
                        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'visible'           => true,
                        'required'          => false,
                        'user_defined'      => false,
                        'searchable'        => false,
                        'filterable'        => false,
                        'comparable'        => false,
                        'visible_on_front'  => false,
                        'visible_in_advanced_search' => false,
                        'unique'            => false
                    ),
                )
            )
        );
    
    }

    public function updateToV1()
    {
        try {
            $this->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'layoutbuddy_enabled', array(
                'group'             => self::GROUP_NAME,
                'label'             => 'Enable LayoutBuddy',
                'type'              => 'int',
                'default'           => '0',
                'backend'           => '',
                'frontend'          => '',
                'input'             => 'select',
                'class'             => '',
                'source'            => 'eav/entity_attribute_source_boolean',
                'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                'visible'           => true,
                'required'          => false,
                'user_defined'      => false,
                'searchable'        => false,
                'filterable'        => false,
                'comparable'        => false,
                'visible_on_front'  => false,
                'unique'            => false,
                'apply_to'          => '',
                'visible_in_advanced_search' => false,
                'is_configurable'   => false
            ));
        } catch (Exception $e) {
            
        }

        // create group
        $entityTypeId = $this->getEntityTypeId(Mage_Catalog_Model_Product::ENTITY);
        $attributeSetId  = $this->getDefaultAttributeSetId($entityTypeId);
        $this->addAttributeGroup($entityTypeId, $attributeSetId, self::GROUP_NAME);
        $groupId = $this->getAttributeGroupId($entityTypeId, $attributeSetId, self::GROUP_NAME);

        // move attribtues to group
        foreach (array('layoutbuddy_enabled', 'layoutbuddy_config_id', 'layoutbuddy_layout_id') as $index => $code) {
            $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, $code);
            $this->addAttributeToGroup($entityTypeId, $attributeSetId, $groupId, $attributeModel->getId(), $index * 10);    
        }
        
    }

    public function changeConfigIdFieldToSelect()
    {
        $this->updateAttribute('catalog_product', 'layoutbuddy_config_id', array(
            'frontend_input' => 'select',
            'source_model' => 'layoutable/system_configid',
            'frontend_label' => 'LayoutBuddy Configuration',
        ));
    }

}
