<?php

class LayoutBuddy_Layoutable_Model_Api
{
    
    protected $_website = null;

    public function setWebsite($website)
    {
        $this->_website = $website;
        return $this;
    }

    public function getWebsite()
    {
        return $this->_website;
    }

    public function buildSignedParamArray($method, $params, $publicKey, $privateKey)
    {
        // make UTC timestamp
        $date = new Zend_Date();
        $timestamp = $date->get(Zend_Date::TIMESTAMP);
        
        $params['public_key'] = $publicKey;
        $params['timestamp'] = $timestamp;
        
        if (isset($params['signature'])) unset($params['signature']);
        
        // generate hash
        ksort($params);
        $message = $method . '?' . http_build_query($params, '', '&');
        $signature = hash_hmac('sha1', $message, $privateKey, false);
        
        // add signature
        $params['signature'] = $signature;
        
        return $params;
    }

    public function request($method, $params = array(), $auth = false, $postParams = null)
    {
        $client = new Zend_Http_Client(LayoutBuddy_Layoutable_Model_Config::getApiUrl() . $method);
        
        if ($this->getWebsite()) {
            $privateKey = $this->_website->getConfig('layoutable/account/private_key');
            $publicKey = $this->_website->getConfig('layoutable/account/public_key');
        } else {
            $privateKey = Mage::getStoreConfig('layoutable/account/private_key');
            $publicKey = Mage::getStoreConfig('layoutable/account/public_key');
        }
        
        // If authentication required, prepare signed request
        if ($auth) {
            $params = $this->buildSignedParamArray($method, $params, $publicKey, $privateKey);
        } else {
            $params['public_key'] = $publicKey;
        }
        
        // Set parameters
        foreach ($params as $k => $v) {
            $client->setParameterGet($k, $v);
        }
        if ($postParams) {
            foreach ($postParams as $k => $v) {
                if (is_array($v) || is_object($v)) {
                    // objects and array are sent as json
                    $client->setParameterPost($k, json_encode($v));
                } else {
                    $client->setParameterPost($k, $v);
                }
            }
        }
        
        // Fire request
        if ($postParams) {
            $response = $client->request(Zend_Http_Client::POST);
        } else {
            $response = $client->request(Zend_Http_Client::GET);
        }
        return $response->getBody();
    }

    public function jsonRequest($method, $params = array(), $auth = false, $postParams = null)
    {
        return Zend_Json::decode($this->request($method, $params, $auth, $postParams));
    }

}
