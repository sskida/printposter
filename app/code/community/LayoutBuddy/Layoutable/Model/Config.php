<?php

class LayoutBuddy_Layoutable_Model_Config
{
    
    const TYPE_ORIGINAL = 'o';
    const TYPE_THUMBNAIL = 't';
    const TYPE_PREVIEW = 'm';
    
    const URL = 'http://tool.layoutbuddy.com/';
    const API_URL = 'http://layoutbuddy.com/api/';
    const JS_URL = '//layoutbuddy.com/js/api/widget.js';
    const JS_EMBED_URL = '//tool.layoutbuddy.com/embed/';   // supports ssl
    const STAT_URL = 'https://s3-eu-west-1.amazonaws.com/static.layoutbuddy.com/';
    
    const SANDBOX_URL = 'http://tool.sandbox.layoutbuddy.com/';
    const SANDBOX_API_URL = 'http://sandbox.layoutbuddy.com/api/';
    const SANDBOX_JS_URL = 'http://sandbox.layoutbuddy.com/js/api/widget.js';
    const SANDBOX_JS_EMBED_URL = 'http://tool.sandbox.layoutbuddy.com/embed/';
    const SANDBOX_STAT_URL = 'https://s3-eu-west-1.amazonaws.com/static.sandbox.layoutbuddy.com/';
    
    protected static function getSubfolderPath($filename)
    {
        $firstChar = substr($filename, 0, 1);
        $secondChar = substr($filename, 1, 1);
        return $firstChar . '/' . $secondChar . '/' . $filename;
    }

    public static function getThumbnailImage($layoutId)
    {
        return self::getStaticUrl() . '120/' . self::getSubfolderPath($layoutId) . '.jpg';
    }

    public static function getPreviewImage($layoutId)
    {
        return self::getStaticUrl() . '600/' . self::getSubfolderPath($layoutId) . '.jpg';
    }
    
    public static function getUrl()
    {
        if (Mage::getStoreConfig('layoutable/account/environment')) {
            return self::SANDBOX_URL;
        } else {
            return self::URL;
        }
    }
    
    public static function getApiUrl()
    {
        if (Mage::getStoreConfig('layoutable/account/environment')) {
            return self::SANDBOX_API_URL;
        } else {
            return self::API_URL;
        }
    }

    public static function getJsUrl()
    {
        if (Mage::getStoreConfig('layoutable/account/environment')) {
            return self::SANDBOX_JS_URL;
        } else {
            return self::JS_URL;
        }
    }
    
    public static function getJsEmbedUrl()
    {
        if (Mage::getStoreConfig('layoutable/account/environment')) {
            return self::SANDBOX_JS_EMBED_URL;
        } else {
            return self::JS_EMBED_URL;
        }
    }

    public static function getStaticUrl()
    {
        if (Mage::getStoreConfig('layoutable/account/environment')) {
            return self::SANDBOX_STAT_URL;
        } else {
            return self::STAT_URL;
        }
    }
    
}
