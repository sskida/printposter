<?php

class LayoutBuddy_Layoutable_Block_Catalog_Product_View_Embed extends Mage_Catalog_Block_Product_View_Type_Simple
{

    private $_configId = '';

    public function setConfigId($id)
    {
        $this->_configId = $id;
    }

    public function getConfigId()
    {
        return $this->_configId;
    }
    
    public function getInitWithId()
    {
        $initWidthId = '';
        $request = $this->getRequest();
        // check if should be edited from cart
        if (
            ($request->getModuleName() == 'checkout') &&
            ($request->getControllerName() == 'cart') &&
            ($request->getActionName() == 'configure')
        ) {
            if ($id = (int)$this->getRequest()->getParam('id')) {
                $cart = Mage::getSingleton('checkout/cart');
                $quoteItem = $cart->getQuote()->getItemById($id);
                $initWidthId = Mage::helper('layoutable')->getLayoutId($quoteItem);
            }
            
        // check if should be edited from wishlist
        } else if (
            ($request->getModuleName() == 'wishlist') &&
            ($request->getControllerName() == 'index') &&
            ($request->getActionName() == 'configure')
        ) {
            if ($item = Mage::registry('wishlist_item')) {
                $buyRequest = $item->getBuyRequest();
                $initWidthId = $buyRequest->getLayoutbuddyId();
            }
        }

        if (!$initWidthId) {
            return $this->getProduct()->getLayoutbuddyLayoutId();
        }
        
        return $initWidthId;
    }

    public function getPublicKey()
    {
        return Mage::getStoreConfig('layoutable/account/public_key');
    }

    public function getPrivateKey()
    {
        return Mage::getStoreConfig('layoutable/account/private_key');
    }
    
    public function getHost()
    {
        return LayoutBuddy_Layoutable_Model_Config::getUrl();
    }
    
    public function displayHost()
    {
        if (Mage::getStoreConfig('layoutable/account/environment')) {
            return true;
        } else {
            return false;
        }
    }

}