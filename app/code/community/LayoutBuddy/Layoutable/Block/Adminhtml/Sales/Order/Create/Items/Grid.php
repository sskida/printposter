<?php

class LayoutBuddy_Layoutable_Block_Adminhtml_Sales_Order_Create_Items_Grid extends Mage_Adminhtml_Block_Sales_Order_Create_Items_Grid
{
    /*
    * override to modify "Configure" button
    */

    public function getConfigureButtonHtml($item)
    {

        if (Mage::helper('layoutable')->isLayoutBuddyItem($item)) {

            $layoutId = Mage::helper('layoutable')->getLayoutId($item);
            $itemId = $item->getId();
            $options = array(
                'label' => Mage::helper('layoutable')->__('Edit layout'),
                'before_html' => sprintf('<span style="white-space: nowrap"><img valign="middle" src="%s" style="width: 60px" /> ', LayoutBuddy_Layoutable_Model_Config::getThumbnailImage($layoutId)),
                'after_html' => sprintf('</span><input type="hidden" id="item_layoutbuddy_id_%s" name="item[%s][layoutbuddy_id]" value="" />', $itemId, $itemId),
                'onclick' => sprintf("layoutbuddyEditLayout('%s', '%s')", $layoutId, $itemId),
            );

            return $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData($options)
                ->toHtml();
        } else {
            return parent::getConfigureButtonHtml($item);
        }

    }

}
