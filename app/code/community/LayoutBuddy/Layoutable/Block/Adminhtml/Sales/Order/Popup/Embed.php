<?php

class LayoutBuddy_Layoutable_Block_Adminhtml_Sales_Order_Popup_Embed extends LayoutBuddy_Layoutable_Block_Catalog_Product_View_Embed
{

    public function getQuoteItemId()
    {
        return $this->getRequest()->getParam('quote_item_id');
    }

    public function getConfigId()
    {
        if ($itemId = $this->getQuoteItemId()) {
            if ($item = Mage::getModel('sales/quote_item')->load($itemId)) {
                if ($product = Mage::getModel('catalog/product')->load($item->getProductId())) {
                    return $product->getLayoutbuddyConfigId();
                }    
            }
        }
        return null;
    }

    public function getInitWithId()
    {
        return $this->getRequest()->getParam('layout_id');
    }

}