<?php

class LayoutBuddy_Layoutable_Block_Checkout_Cart_Item_Configure extends Mage_Checkout_Block_Cart_Item_Configure
{

    protected function _prepareLayout()
    {
        $result = parent::_prepareLayout();
        // Set custom template with 'Update Cart' button
        if ($block = $this->getLayout()->getBlock('product.info.addtocart')) {
            $block->setTemplate('layoutable/checkout/cart/item/configure/updatecart.phtml');
        }
        return $result;
    }

}
