<?php

class LayoutBuddy_Layoutable_Block_Checkout_Cart_Item_Renderer extends Mage_Checkout_Block_Cart_Item_Renderer
{
    
    public function getProductThumbnail()
    {
        if ($image = $this->helper('layoutable/image')->init($this->getItem(), 'thumbnail')) {
            return $image;
        } else {
            return parent::getProductThumbnail();
        }
    }
    
}