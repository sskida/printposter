<?php

class Customer_Feedback_Block_Adminhtml_Feedback_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('feedback_form', array('legend'=>Mage::helper('feedback')->__('Item information')));
        
        $fieldset->addField('name', 'text', array(
            'label'     => Mage::helper('feedback')->__('Name'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'name',
        ));
        $fieldset->addField('city', 'text', array(
            'label'     => Mage::helper('feedback')->__('City'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'city',
        ));

        $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('feedback')->__('Status'),
            'name'      => 'status',
            'values'    => array(
                array(
                    'value'     => 1,
                    'label'     => Mage::helper('feedback')->__('Active'),
                ),

                array(
                    'value'     => 0,
                    'label'     => Mage::helper('feedback')->__('Inactive'),
                ),
            ),
        ));
        
        $fieldset->addField('comment', 'editor', array(
            'name'      => 'comment',
            'label'     => Mage::helper('feedback')->__('Content'),
            'title'     => Mage::helper('feedback')->__('Content'),
            'style'     => 'width:98%; height:400px;',
            'wysiwyg'   => false,
            'required'  => true,
        ));
        
        if ( Mage::getSingleton('adminhtml/session')->getFeedbackData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getFeedbackData());
            Mage::getSingleton('adminhtml/session')->setFeedbackData(null);
        } elseif ( Mage::registry('feedback_data') ) {
            $form->setValues(Mage::registry('feedback_data')->getData());
        }
        return parent::_prepareForm();
    }
}