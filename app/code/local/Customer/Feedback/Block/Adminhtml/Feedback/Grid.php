<?php

class Customer_Feedback_Block_Adminhtml_Feedback_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('feedbackGrid');
        // This is the primary key of the database
        $this->setDefaultSort('feedback_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('feedback/feedback')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('feedback_id', array(
            'header'    => Mage::helper('feedback')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'feedback_id',
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('feedback')->__('Name'),
            'align'     =>'left',
            'index'     => 'name',
        ));
         $this->addColumn('city', array(
            'header'    => Mage::helper('feedback')->__('City'),
            'align'     =>'left',
            'index'     => 'city',
        ));
        $this->addColumn('comment', array(
            'header'    => Mage::helper('feedback')->__('Comment'),
            'align'     =>'left',
            'index'     => 'comment',
        ));
        /*
        $this->addColumn('content', array(
            'header'    => Mage::helper('<module>')->__('Item Content'),
            'width'     => '150px',
            'index'     => 'content',
        ));
        */

        $this->addColumn('created_time', array(
            'header'    => Mage::helper('feedback')->__('Creation Time'),
            'align'     => 'left',
            'width'     => '120px',
            'type'      => 'date',
            'default'   => '--',
            'index'     => 'created_time',
        ));

        $this->addColumn('update_time', array(
            'header'    => Mage::helper('feedback')->__('Update Time'),
            'align'     => 'left',
            'width'     => '120px',
            'type'      => 'date',
            'default'   => '--',
            'index'     => 'update_time',
        ));    


        $this->addColumn('status', array(

            'header'    => Mage::helper('feedback')->__('Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'status',
            'type'      => 'options',
            'options'   => array(
                1 => 'Active',
                0 => 'Inactive',
            ),
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }


}