<?php

class Customer_Feedback_Adminhtml_FeedbackController extends Mage_Adminhtml_Controller_action
{

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('feedback/items')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        return $this;
    }    
    
    public function indexAction() {
        $this->_initAction();        
        $this->_addContent($this->getLayout()->createBlock('feedback/adminhtml_feedback'));
        $this->renderLayout();
    }

    public function editAction()
    {
        $feedbackId     = $this->getRequest()->getParam('id');
        $feedbackModel  = Mage::getModel('feedback/feedback')->load($feedbackId);

        if ($feedbackModel->getId() || $feedbackId == 0) {

            Mage::register('feedback_data', $feedbackModel);

            $this->loadLayout();
            $this->_setActiveMenu('feedback/items');
            
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
            
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            
            $this->_addContent($this->getLayout()->createBlock('feedback/adminhtml_feedback_edit'))
                 ->_addLeft($this->getLayout()->createBlock('feedback/adminhtml_feedback_edit_tabs'));
                
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('feedback')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }
    
    public function newAction()
    {
        $this->_forward('edit');
    }
    
    public function saveAction()
    {
        if ( $this->getRequest()->getPost() ) {
            try {
                $postData = $this->getRequest()->getPost();
                $feedbackModel = Mage::getModel('feedback/feedback');
                
                $feedbackModel->setId($this->getRequest()->getParam('id'))
                    ->setName($postData['name'])
                    ->setCity($postData['city'])
                    ->setComment($postData['comment'])
                    ->setStatus($postData['status'])
                    ->setCreated_time(Mage::getModel('core/date')->date('Y-m-d H:i:s'))
                    ->setUpdate_time(Mage::getModel('core/date')->date('Y-m-d H:i:s'))
                    ->save();
                
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully saved'.$postData['name']));
                Mage::getSingleton('adminhtml/session')->setfeedbackData(false);

                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setfeedbackData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }
    
    public function deleteAction()
    {
        if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $feedbackModel = Mage::getModel('feedback/feedback');
                
                $feedbackModel->setId($this->getRequest()->getParam('id'))
                    ->delete();
                    
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }
}