<?php
class Chiragdodia_Mymodule_IndexController extends Mage_Core_Controller_Front_Action
{
    public function imageuploadAction()
    {

        $array = array('sucess'=>0, 'msg'=>'', 'product_id'=>0, 'url'=>'');



        $category_id= $_POST['category_id']; //*
        $order_type= $_POST['order_type']; 
        $canvas_type= $_POST['canvas_type'];
        $media=$_POST['media'];
        $type=$_POST['type'];
        $order_size= $_POST['size'];
        $size_price=$_POST['size_price'];
        $frame=$_POST['frame'];
        $view=$_POST['view'];
        $effects=$_POST['effects'];
        $rotation=$_POST['rotation'];
        $brightness=$_POST['brightness'];
        $contrast=$_POST['contrast'];
        $prj_name= $_POST['prj_name'];  //*
        $prj_comments= $_POST['prj_comments']; //*
        $price= $_POST['price'];
        $three_five_frm_cost=$_POST['three_five_frm_cost'];
        $frame_price= $_POST['frame_price'];
        $effects_price=$_POST['effects_price'];
        $digitalcorrections= $_POST['digitalcorrections'];
        $major_retouching=$_POST['major_retouching'];
        $minor_change_array=$_POST['minor_change_array'];

       $total_cost=floatval($price)+floatval($frame_price)+floatval($effects_price)+floatval($extra_charge);

        //images 
        $filename1           = time()."_original.jpg";
        $filepath1           = Mage::getBaseDir('media') . DS . 'import'. DS . $filename1; //path for temp storage folder: ./media/import/
        $original_img= $_POST['originalimg'];
        $original_img = str_replace('data:image/png;base64,', '', $original_img);
        $original_img = str_replace(' ', '+', $original_img);
        $data = base64_decode($original_img);
        file_put_contents($filepath1, $data);

        $filename2           = time()."_original.jpg";
        $filepath2          = Mage::getBaseDir('media') . DS . 'import'. DS . $filename2; //path for temp storage folder: ./media/import/
        $edited_img= $_POST['editedimg'];
        $edited_img = str_replace('data:image/png;base64,', '', $edited_img);
        $edited_img = str_replace(' ', '+', $edited_img);
        $data1 = base64_decode($edited_img);
        file_put_contents($filepath2, $data1);


Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID); //
        $product = Mage::getModel('catalog/product');
        $rand = rand(1, 9999);
        $product
        ->setTypeId('simple')
        ->setAttributeSetId(9) // default attribute set
        ->setSku('product' . $rand) // generate a random SKU
        ->setWebsiteIDs(array(1));
$product
        ->setCategoryIds(array(intval($category_id)))
        ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
        ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH); // visible in catalog and search;

$product->setStockData(array(
            'use_config_manage_stock' => 0, // use global config?
            'manage_stock'            => 1, // should we manage stock or not?
            'is_in_stock'             => 1,
            'qty'                     => 1,
        ));
$product
            ->setName($prj_name) // add string attribute
            ->setShortDescription($prj_comments) // add text attribute
 
            // set up prices
            ->setPrice($total_cost)

            ->setTaxClassId(2)    // Taxable Goods by default
           // ->setWeight(87)

 
// ->setBlend(false)
;









//custom fields
$product
    ->setOrder_type($order_type)
    ->setCanvas_type($canvas_type)
    ->setType($type)
    ->setOrder_size($order_size)
    ->setSize_price($size_price)
    ->setFrame($frame)
    ->setView($view)
    ->setEffects($effects)
    ->setRotation($rotation)
    ->setBrightness($brightness)
    ->setContrast($contrast)
    ->setSize_price($price)
    ->setFrame_price($frame_price)
    ->setEffects_price($effects_price)
    ->setDigital_corrections($digitalcorrections)
    ->setMedia($media)
    ->setThree_five_frm_cost($three_five_frm_cost)
    ->setMajor_retouching($major_retouching)
    ->setMinor_change_array($minor_change_array)
  ;



$mediaArray = array(
    'thumbnail'   => $filename1,
    'image' => $filename2,
);


// Remove unset images, add image to gallery if exists
$importDir = Mage::getBaseDir('media') . DS . 'import/';

foreach($mediaArray as $imageType => $fileName) {
    $filePath = $importDir.$fileName;
    if ( file_exists($filePath) ) {
        try {
            $product->addImageToMediaGallery($filePath, $imageType, false);
        } catch (Exception $e) {
            $array['sucess']=0;
            $array['msg']=$e->getMessage();
           echo json_encode($array);
        }
    } else {
        echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
    }
}
        ;

$product->save();

$array['sucess']=0;
$array['product_id']=$product->getId();

 $_product = Mage::getModel('catalog/product')->load($product->getId());
 $_url = Mage::helper('checkout/cart')->getAddUrl($_product);

$finalurl= str_replace('\\', '', $_url);



$array['url']= $finalurl;

//echo $finalurl;
echo json_encode($array);

}





    public function imagecropAction()
    {
       
        $src = $_POST['img'];

        $pos  = strpos($src, ';');
        $mime_type = explode(':', substr($src, 0, $pos))[1];

        switch ($mime_type)
        {
            case 'image/jpeg':
                $img_r = imagecreatefromjpeg($src);
            break;
            case 'image/gif':
                $img_r = imagecreatefromgif($src);
            break;
            case 'image/png':
                $img_r = imagecreatefrompng($src);
            break;
            default:
                die('Invalid image type');
        }


       // $img_r = imagecreatefromjpeg($src);
        $dst_r = ImageCreateTrueColor( $_POST['w'], $_POST['h'] );

        imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
        $_POST['w'],$_POST['h'],$_POST['w'],$_POST['h']);


        ob_start(); 
           
         switch ($mime_type)
        {
            case 'image/jpeg':
                imagejpeg($dst_r); 
            break;
            case 'image/gif':
                imagegif($dst_r); 
            break;
            case 'image/png':
                imagepng($dst_r); 
            break;
            default:
                die('Invalid image type');
        }

         $image_data = ob_get_contents(); 
         ob_end_clean(); 
        imagedestroy($dst_r);
        imagedestroy($img_r);


          switch ($mime_type)
        {
            case 'image/jpeg':

                header("Content-type: image/jpeg");
                echo 'data:image/jpeg;base64,' . base64_encode($image_data);

            break;
            case 'image/gif':

                header("Content-type: image/gif");
                echo 'data:image/gif;base64,' . base64_encode($image_data);

            break;
            case 'image/png':

                header("Content-type: image/png");
                echo 'data:image/png;base64,' . base64_encode($image_data); 

            break;
            default:
                die('Invalid image type');
        }


    }


    public function getvinylsizeAction()
    {
       

  $array1 = array();
  $type = $_POST['type'];
  $cat_id = $_POST['catid'];
  
  


  $collection = Mage::getModel('catalog/category')->getCategories(intval($cat_id));
  $helper     = Mage::helper('catalog/category');

  foreach ($collection as $cat):

      $childLevel2Category = Mage::getModel('catalog/category')->getCategories($cat->entity_id);
                 
          foreach ($childLevel2Category as $catLevel2) {

              $chcategory=Mage::getModel('catalog/category')->load($catLevel2->getId());


                if(strcmp($type,$chcategory->getName())==0){
                    
                    $array1[] = array("size" => str_replace('"','',$cat->getName()), "price" => (floatval($chcategory->getDescription())));
                } 
                    

          } 

        
  endforeach;


    echo json_encode($array1);
  

    }


 public function loginuserAction()
    {


        if(!Mage::getSingleton('customer/session')->isLoggedIn())
      {

        $email = $_POST['email'];
        $password = $_POST['password'];
        $pth=Mage::getBaseDir('app').'/Mage.php';
        require_once($pth); //Path to Magento umask(0); 
        umask(0);
        Mage::app();
        $customer = Mage::getModel('customer/customer');
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());

        

        try {
            $customSession= Mage::getSingleton('customer/session');
            $customSession->login($email, $password);
            $customer = $customSession->getCustomer();
            $customSession->setCustomerAsLoggedIn($customer);
            $data['status'] = 1;
            $data['msg'] ='login Successfull !!';
            echo json_encode($data);
            
        } catch(Exception $ex) {
          //  $this->_result .= 'wronglogin,';
            $data['status'] = 0;
            $data['msg'] =$ex->getMessage();
            echo json_encode($data);
        }




    }else{
            $data['status'] = 0;
            $data['msg'] ='already logged in';
            echo json_encode($data);
    }





    }





    public function vinylorderAction()
    {

        $array = array('sucess'=>0, 'msg'=>'', 'product_id'=>0, 'url'=>'');



        $category_id= $_POST['category_id']; //*
        $order_type= $_POST['order_type']; 
        $media=$_POST['media'];
        $type=$_POST['type'];
        $order_size= $_POST['size'];
        $price= $_POST['price'];
        $quantity=$_POST['quantity'];
        $prj_comments=$_POST['prj_comments'];
        $jsonrresp=$_POST['resps'];
        //images       
//echo var_dump($jsonrresp);
//exit();
           //images 
        $filename1           = time()."_original.jpg";
        $filepath1           = Mage::getBaseDir('media') . DS . 'import'. DS . $filename1; //path for temp storage folder: ./media/import/
        $original_img= $_POST['originalimg'];
        $original_img = str_replace('data:image/png;base64,', '', $original_img);
        $original_img = str_replace(' ', '+', $original_img);
        $data = base64_decode($original_img);
        file_put_contents($filepath1, $data);


Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID); //
        $product = Mage::getModel('catalog/product');
        $rand = rand(1, 9999);
        $product
        ->setTypeId('simple')
        ->setAttributeSetId(9) // default attribute set
        ->setSku('product' . $rand) // generate a random SKU
        ->setWebsiteIDs(array(1));
$product
        ->setCategoryIds(array(intval($category_id)))
        ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
        ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH); // visible in catalog and search;

$product->setStockData(array(
            'use_config_manage_stock' => 0, // use global config?
            'manage_stock'            => 1, // should we manage stock or not?
            'is_in_stock'             => 1,
            'qty'                     => $quantity,
        ));
$product
            ->setName('vinyal order' . $rand) // add string attribute
            ->setShortDescription($prj_comments) // add text attribute
             ->setLamination($lamination)
            // set up prices
            ->setPrice($price)

            ->setTaxClassId(2)    // Taxable Goods by default
           // ->setWeight(87)

;





$obj = json_decode($jsonrresp);


//custom fields
$product
    ->setOrder_type($order_type)
    ->setType($type)
    ->setOrder_size($order_size)
    ->setServerpath($obj[0]->{'serverpath'})
    ->setFilepath($obj[0]->{'filepath'})
    ->setSize_price($price)
    ;


$mediaArray = array(
    'thumbnail'   => $filename1,
);


// Remove unset images, add image to gallery if exists
$importDir = Mage::getBaseDir('media') . DS . 'import/';

foreach($mediaArray as $imageType => $fileName) {
    $filePath = $importDir.$fileName;
    if ( file_exists($filePath) ) {
        try {
            $product->addImageToMediaGallery($filePath, $imageType, false);
        } catch (Exception $e) {
            $array['sucess']=0;
            $array['msg']=$e->getMessage();
           echo json_encode($array);
        }
    } else {
        echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
    }
}
        ;


$product->save();

$array['sucess']=0;
$array['product_id']=$product->getId();

 $_product = Mage::getModel('catalog/product')->load($product->getId());
 $_url = Mage::helper('checkout/cart')->getAddUrl($_product);

$finalurl= str_replace('\\', '', $_url);



$array['url']= $finalurl;

//echo $finalurl;
echo json_encode($array);

}




 public function upldvinylAction()
    {
  

$filenm=time().$_FILES['file']['name'];
$target_path = Mage::getBaseDir('media') . DS . 'import/' . $filenm; 
$serverpth=Mage::getBaseUrl('media'). "import/" . $filenm;



 if ( 0 < $_FILES['file']['error'] ) {
        $array1[] = array("status" => 0);
    }
    else {
        if(move_uploaded_file($_FILES['file']['tmp_name'], $target_path)){
            $array1[] = array("status" => 1, "filepath" => $target_path, "filename" => $filenm, "serverpath" => $serverpth);
        }else{
            $array1[] = array("status" => 0);
        }
    }


echo json_encode($array1);
 







    }


  public function getsizeAction()
    {   

  $array1 = array();
  $parent = $_POST['parent'];
  $type = $_POST['type'];
  $cat_id = $_POST['catid'];
  
  


  $collection = Mage::getModel('catalog/category')->getCategories(intval($cat_id));
  $helper     = Mage::helper('catalog/category');

  foreach ($collection as $cat):

      $childLevel2Category = Mage::getModel('catalog/category')->getCategories($cat->entity_id);
                 
          foreach ($childLevel2Category as $catLevel2) {

              $chcategory=Mage::getModel('catalog/category')->load($catLevel2->getId());

                 $pricesdiff=0;
                if(strcmp($type,$chcategory->getName())==0){
                    if($parent=='three frame canvas'){
                        $pricesdiff=floatval($chcategory->getDescription())*0.10;
                        $array1[] = array("size" => str_replace('"','',$cat->getName()), "price" => (floatval($chcategory->getDescription())+$pricesdiff));
                    }else if($parent=='five frame canvas'){
                        $pricesdiff=floatval($chcategory->getDescription())*0.30;
                        $array1[] = array("size" => str_replace('"','',$cat->getName()), "price" => (floatval($chcategory->getDescription())+$pricesdiff));
                    }else{
                        $array1[] = array("size" => str_replace('"','',$cat->getName()), "price" => (floatval($chcategory->getDescription())));
                    }
                    
                } 
                    

          } 

        
  endforeach;


    echo json_encode($array1);
  

    }


}