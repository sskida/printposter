<?php
class Chiragdodia_Mymodule_IndexController extends Mage_Core_Controller_Front_Action
{
    public function imageuploadAction()
    {
        $name = $_FILES['img']['name'];
		$tmp = $_FILES['img']['tmp_name'];
		$type = $_FILES['img']['type'];
		$new_name = time()."_".$_FILES['img']['name'];
		
		if($type == "image/jpeg" || $type == "image/jpg" || $type == "image/pjpeg")
		{
				$data = file_get_contents($_FILES['img']['tmp_name']);
				$data = base64_encode($data);
			//	$_SESSION["original_image"]=$data;
			//echo "<img src=\"data:image/jpeg;base64,".$data."\"/>";
			 $newName = time() . '_' . $_FILES['img']['name'];
			 $destination =  'uploads/original/'.$newName;
			 echo $destination;
			 if (move_uploaded_file($_FILES['img']['tmp_name'], $destination)) {
			 		$_SESSION['original_file']=$newName;
			 		$arr = array('success' => 1);
   					echo json_encode($arr);
		           
		        }else{
		        	echo $_FILES['img']['error'];
		        }
		}
		else{
			echo err("Format not supported");
		}		
    }


    public function resizeimageAction()
    {	
    	if(isset($_SESSION['edited_file'])){
    		$filepath="uploads/edited/".$_SESSION['edited_file'];
    	}else{
    		$filepath="uploads/original/".$_SESSION['original_file'];
    	}
		$nwidth=$_POST['width'];
		$nheight=$_POST['height'];
		// Content type
		header('Content-Type: image/jpeg');

		// Get new sizes
		list($width, $height) = getimagesize($filepath);
		$newwidth = intval($nwidth) * 96;
		$newheight = intval($nheight) * 96;

		// Load
		$thumb = imagecreatetruecolor($newwidth, $newheight);
		$source = imagecreatefromjpeg($filepath);

		// Resize
		imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

		// Output and free memory
		//the resized image will be 400x300
		
		ob_start (); 

		   imagejpeg($thumb);
		  $image_data = ob_get_contents (); 
		  
		ob_end_clean (); 
		imagedestroy($thumb);
		$image_data_base64 = base64_encode ($image_data);

		$_SESSION['tempht']=intval($nheight)*4.8959;
		$_SESSION['tempwdt']=intval($nwidth)*4.8959;
		

		if(isset($_SESSION['color'])){

		$finishing=$_SESSION['color'];

			 if($finishing!=12){
    	$imageFilters = array (
				1 => array (IMG_FILTER_NEGATE, 0),
				2 => array (IMG_FILTER_GRAYSCALE, 0),
				3 => array (IMG_FILTER_BRIGHTNESS, 1),
				4 => array (IMG_FILTER_CONTRAST, 1),
				5 => array (IMG_FILTER_COLORIZE, 4),
				6 => array (IMG_FILTER_EDGEDETECT, 0),
				7 => array (IMG_FILTER_EMBOSS, 0),
				8 => array (IMG_FILTER_GAUSSIAN_BLUR, 0),
				9 => array (IMG_FILTER_SELECTIVE_BLUR, 0),
				10 => array (IMG_FILTER_MEAN_REMOVAL, 0),
				11 => array (IMG_FILTER_SMOOTH, 0),
			);
    
			if( $thumb && $finishing == 3){

			imagefilter($thumb, $imageFilters[$finishing][0],30);


		
			ob_start (); 
			  imagejpeg($thumb);
			  $image_data = ob_get_contents (); 
			ob_end_clean (); 
			imagedestroy($thumb);
			$image_data_base64 = base64_encode ($image_data);
		echo "<img src=\"data:image/jpeg;base64,".$image_data_base64."\" height=\"".$_SESSION['tempht']."\" width=\"".$_SESSION['tempwdt']."\" />";



			}else if( $thumb && $finishing == 5){
				imagefilter($thumb, $imageFilters[$finishing][0],0, -155, -255);

			ob_start (); 
			  imagejpeg($thumb);
			  $image_data = ob_get_contents (); 
			ob_end_clean (); 
			imagedestroy($thumb);
			$image_data_base64 = base64_encode ($image_data);
		echo "<img src=\"data:image/jpeg;base64,".$image_data_base64."\" height=\"".$_SESSION['tempht']."\" width=\"".$_SESSION['tempwdt']."\" />"; 


			}else if( $thumb && $finishing == 4){
				imagefilter($thumb, $imageFilters[$finishing][0],60);
		
			ob_start (); 
			  imagejpeg($thumb);
			  $image_data = ob_get_contents (); 
			ob_end_clean (); 
			imagedestroy($thumb);
			$image_data_base64 = base64_encode ($image_data);
		echo "<img src=\"data:image/jpeg;base64,".$image_data_base64."\" height=\"".$_SESSION['tempht']."\" width=\"".$_SESSION['tempwdt']."\" />";


			}else if( $thumb && $finishing == 11){
				imagefilter($thumb, $imageFilters[$finishing][0],20);
			ob_start (); 
			  imagejpeg($thumb);
			  $image_data = ob_get_contents (); 
			ob_end_clean (); 
			imagedestroy($thumb);
			$image_data_base64 = base64_encode ($image_data);
		echo "<img src=\"data:image/jpeg;base64,".$image_data_base64."\" height=\"".$_SESSION['tempht']."\" width=\"".$_SESSION['tempwdt']."\" />";


			}else if($thumb) {
				imagefilter($thumb, $imageFilters[$finishing][0]);
			
				ob_start (); 
			  imagejpeg($thumb);
			  $image_data = ob_get_contents (); 
			ob_end_clean (); 
			imagedestroy($thumb);
			$image_data_base64 = base64_encode ($image_data);
		echo "<img src=\"data:image/jpeg;base64,".$image_data_base64."\" height=\"".$_SESSION['tempht']."\" width=\"".$_SESSION['tempwdt']."\" />";
		}

		$newName = time() . '_editedfile.jpg';
			 $destination =  'uploads/edited/'.$newName;
		
			$_SESSION['edited_file']=$newName;
			imagejpeg($im,$destination);

	}
		
	}else{


		$newName = time() . '_editedfile.jpg';
			 $destination =  'uploads/edited/'.$newName;
			 imagejpeg($thumb,$destination);
			 $_SESSION['edited_file']=$newName;



			echo "<img src=\"data:image/jpeg;base64,".$image_data_base64."\" height=\"".intval($nheight)*4.8959."\" width=\"".intval($nwidth)*4.8959."\" />";
   
	}





}


	
    public function colorfinishingAction(){
    	$finishing= $_POST['finishing'];

    	if(isset($_SESSION['original_file'])){
    		$filepath="uploads/original/".$_SESSION['original_file'];
    	}else{
    		$arr = array('success' => 0, 'msg' => 'Please Select size first!!!');
   					echo json_encode($arr);
    	}

    	$im = imagecreatefromjpeg($filepath);
    
    if($finishing!=12){
    	$imageFilters = array (
				1 => array (IMG_FILTER_NEGATE, 0),
				2 => array (IMG_FILTER_GRAYSCALE, 0),
				3 => array (IMG_FILTER_BRIGHTNESS, 1),
				4 => array (IMG_FILTER_CONTRAST, 1),
				5 => array (IMG_FILTER_COLORIZE, 4),
				6 => array (IMG_FILTER_EDGEDETECT, 0),
				7 => array (IMG_FILTER_EMBOSS, 0),
				8 => array (IMG_FILTER_GAUSSIAN_BLUR, 0),
				9 => array (IMG_FILTER_SELECTIVE_BLUR, 0),
				10 => array (IMG_FILTER_MEAN_REMOVAL, 0),
				11 => array (IMG_FILTER_SMOOTH, 0),
			);
    
			if( $im && $finishing == 3){

			imagefilter($im, $imageFilters[$finishing][0],30);


		
			ob_start (); 
			  imagejpeg($im);
			  $image_data = ob_get_contents (); 
			ob_end_clean (); 
			imagedestroy($im);
			$image_data_base64 = base64_encode ($image_data);
		echo "<img src=\"data:image/jpeg;base64,".$image_data_base64."\" height=\"".$_SESSION['tempht']."\" width=\"".$_SESSION['tempwdt']."\" />";



			}else if( $im && $finishing == 5){
				imagefilter($im, $imageFilters[$finishing][0],0, -155, -255);

			ob_start (); 
			  imagejpeg($im);
			  $image_data = ob_get_contents (); 
			ob_end_clean (); 
			imagedestroy($im);
			$image_data_base64 = base64_encode ($image_data);
		echo "<img src=\"data:image/jpeg;base64,".$image_data_base64."\" height=\"".$_SESSION['tempht']."\" width=\"".$_SESSION['tempwdt']."\" />"; 


			}else if( $im && $finishing == 4){
				imagefilter($im, $imageFilters[$finishing][0],60);
		
			ob_start (); 
			  imagejpeg($im);
			  $image_data = ob_get_contents (); 
			ob_end_clean (); 
			imagedestroy($im);
			$image_data_base64 = base64_encode ($image_data);
		echo "<img src=\"data:image/jpeg;base64,".$image_data_base64."\" height=\"".$_SESSION['tempht']."\" width=\"".$_SESSION['tempwdt']."\" />";


			}else if( $im && $finishing == 11){
				imagefilter($im, $imageFilters[$finishing][0],20);
			ob_start (); 
			  imagejpeg($im);
			  $image_data = ob_get_contents (); 
			ob_end_clean (); 
			imagedestroy($im);
			$image_data_base64 = base64_encode ($image_data);
		echo "<img src=\"data:image/jpeg;base64,".$image_data_base64."\" height=\"".$_SESSION['tempht']."\" width=\"".$_SESSION['tempwdt']."\" />";


			}else if($im) {
				imagefilter($im, $imageFilters[$finishing][0]);
			
				ob_start (); 
			  imagejpeg($im);
			  $image_data = ob_get_contents (); 
			ob_end_clean (); 
			imagedestroy($im);
			$image_data_base64 = base64_encode ($image_data);
		echo "<img src=\"data:image/jpeg;base64,".$image_data_base64."\" height=\"".$_SESSION['tempht']."\" width=\"".$_SESSION['tempwdt']."\" />";


		$newName = time() . '_editedfile.jpg';
			 $destination =  'uploads/edited/'.$newName;
		
			$_SESSION['edited_file']=$newName;
			imagejpeg($im,$destination);

			$_SESSION['color']=$finishing;



			}else{
				 echo 'Conversion to grayscale failed.';
			}

		imagedestroy($im);


      }else{


      	$filepath="uploads/original/".$_SESSION['original_file'];

      	$nwidth=$_SESSION['tempwdt'];
		$nheight=$_SESSION['tempht'];
		// Content type
		header('Content-Type: image/jpeg');

		// Get new sizes
		list($width, $height) = getimagesize($filepath);
		$newwidth = intval($nwidth) * 96;
		$newheight = intval($nheight) * 96;

		// Load
		$thumb = imagecreatetruecolor($newwidth, $newheight);
		$source = imagecreatefromjpeg($filepath);

		// Resize
		imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

		// Output and free memory
		//the resized image will be 400x300
			$newName = time() . '_editedfile.jpg';
			 $destination =  'uploads/edited/'.$newName;
			 imagejpeg($thumb,  $destination);
		$_SESSION['edited_file']=$newName;
		if (isset($_SESSION['color']))
	    {
	        unset($_SESSION['color']);
	    }

		ob_start (); 

		  imagejpeg($thumb);
		  $image_data = ob_get_contents (); 

		ob_end_clean (); 
		imagedestroy($thumb);
		$image_data_base64 = base64_encode ($image_data);

	
		
		echo "<img src=\"data:image/jpeg;base64,".$image_data_base64."\" height=\"".intval($nheight)*4.8959."\" width=\"".intval($nwidth)*4.8959."\" />";




      }
    }
}
function err($str)
		{
			echo "<div class='error'>".$str."</div>";
		}
		function msg($str)
		{
			echo "<div class='msg'>".$str."</div>";
		}